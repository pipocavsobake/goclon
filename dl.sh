ssh clonclient@shirykalov.ru "/usr/lib/postgresql/10/bin/pg_dump -Fc clonclient -p 5433 --no-owner >/home/clonclient/tmp.bak"
rsync -avz clonclient@shirykalov.ru:/home/clonclient/tmp.bak tmp/tmp.bak
local ops="-dclonclient"
if [[ $1 != "" ]]; then
  ops=$1
fi
pg_restore -Fc $ops tmp/tmp.bak

module gitlab.com/pipocavsobake/goclon

go 1.14

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/buger/jsonparser v1.1.1
	github.com/campoy/embedmd v0.0.0-20171205015432-c59ce00e0296 // indirect
	github.com/containous/go-bindata v1.0.0
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/denisenkom/go-mssqldb v0.0.0-20200620013148-b91950f658ec // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dustin/go-broadcast v0.0.0-20171205050544-f664265f5a66 // indirect
	github.com/elazarl/go-bindata-assetfs v1.0.0 // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/fatih/color v1.9.0 // indirect
	github.com/gin-contrib/static v0.0.1
	github.com/gin-gonic/autotls v0.0.0-20180426091246-be87bd5ef97b // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/glebtv/gin-swagger v0.0.0-20180528194915-1f95673adf6a
	github.com/go-openapi/spec v0.20.4 // indirect
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/go-webpack/webpack v1.5.0
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/golang/lint v0.0.0-20180702182130-06c8688daad7 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/gosimple/slug v1.11.2
	github.com/jessevdk/go-assets v0.0.0-20160921144138-4f4301a06e15 // indirect
	github.com/jinzhu/configor v1.2.1
	github.com/jinzhu/copier v0.3.4 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/kisielk/gotool v1.0.0 // indirect
	github.com/lib/pq v1.10.4 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/manucorporat/stats v0.0.0-20180402194714-3ba42d56d227 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mattn/go-sqlite3 v1.14.0 // indirect
	github.com/microcosm-cc/bluemonday v1.0.16 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/olivere/elastic/v7 v7.0.12
	github.com/pbnjay/strptime v0.0.0-20140226051138-5c05b0d668c9
	github.com/pkg/errors v0.9.1
	github.com/qor/admin v1.2.0
	github.com/qor/assetfs v0.0.0-20170713023933-ff57fdc13a14
	github.com/qor/auth v0.0.0-20200821034127-11d4c974507d
	github.com/qor/mailer v0.0.0-20180329083248-0555e49f99ac // indirect
	github.com/qor/media v0.0.0-20210903074215-02bf646a73bd
	github.com/qor/middlewares v0.0.0-20170822143614-781378b69454 // indirect
	github.com/qor/oss v0.0.0-20191031055114-aef9ba66bf76 // indirect
	github.com/qor/qor v1.2.0
	github.com/qor/redirect_back v0.0.0-20170907030740-b4161ed6f848
	github.com/qor/render v1.1.1
	github.com/qor/responder v0.0.0-20201015104727-4f3a345378c2
	github.com/qor/roles v0.0.0-20201008080147-dcaf8a4646d8 // indirect
	github.com/qor/session v0.0.0-20170907035918-8206b0adab70
	github.com/qor/validations v0.0.0-20171228122639-f364bca61b46
	github.com/qor/worker v0.0.0-20190805090529-35a245417f70
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/sevlyar/go-daemon v0.1.5
	github.com/swaggo/gin-swagger v1.2.0 // indirect
	github.com/swaggo/swag v1.7.6
	github.com/theplant/htmltestingutils v0.0.0-20190423050759-0e06de7b6967 // indirect
	github.com/theplant/testingutils v0.0.0-20190603093022-26d8b4d95c61 // indirect
	github.com/thinkerou/favicon v0.1.0 // indirect
	github.com/ugorji/go v1.2.6 // indirect
	github.com/urfave/cli v1.22.5
	github.com/yosssi/gohtml v0.0.0-20200519115854-476f5b4b8047 // indirect
	golang.org/x/crypto v0.0.0-20211209193657-4570a0811e8b // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/net v0.0.0-20211209124913-491a49abca63 // indirect
	golang.org/x/sys v0.0.0-20211210111614-af8b64212486 // indirect
	golang.org/x/text v0.3.7
	golang.org/x/tools v0.1.8 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/cheggaaa/pb.v1 v1.0.28
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/olivere/elastic.v5 v5.0.86
	gopkg.in/resty.v1 v1.12.0
	gopkg.in/yaml.v2 v2.4.0
)

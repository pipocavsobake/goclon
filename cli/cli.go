package cli

import (
	"log"
	"net/http"
	"os"
	"sort"

	daemon "github.com/sevlyar/go-daemon"
	"github.com/urfave/cli"

	"gitlab.com/pipocavsobake/goclon/app/job"
	"gitlab.com/pipocavsobake/goclon/app/job/dl"
	fr "gitlab.com/pipocavsobake/goclon/app/job/fragments"
	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/app/workers"
	"gitlab.com/pipocavsobake/goclon/assets"
	"gitlab.com/pipocavsobake/goclon/config/admin"
	"gitlab.com/pipocavsobake/goclon/config/logger"
	auth "gitlab.com/pipocavsobake/goclon/config/siteauth"
	"gitlab.com/pipocavsobake/goclon/progress"
	"gitlab.com/pipocavsobake/goclon/sites/ic"
)

var App *cli.App

var Serve bool
var Daemon *daemon.Context
var Sock string
var SWAG_HOST string
var WebpackProd bool

func init() {
	App = cli.NewApp()
	App.EnableBashCompletion = true
	App.Name = "clonclient"
	App.Usage = "AESh server for clonclient App"
	App.Version = "0.0.1"

	cli.VersionFlag = cli.BoolFlag{
		Name:  "version, v",
		Usage: "print only the version",
	}
	App.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "standard-log",
			Usage:  "other log file",
			Value:  "log/standard.log",
			EnvVar: "STANDARD_LOG",
		},
		cli.StringFlag{
			Name:   "sql-log",
			Usage:  "sql log file",
			Value:  "log/sql.log",
			EnvVar: "SQL_LOG",
		},
		cli.BoolFlag{
			Name:  "dev",
			Usage: "run dev tools on localhost:6060",
		},
		cli.StringFlag{
			Name:        "swag-host",
			Usage:       "swag host",
			Value:       "localhost:5292",
			EnvVar:      "SWAG_HOST",
			Destination: &SWAG_HOST,
		},
	}
	App.Before = func(c *cli.Context) error {
		standardLogFile, err := os.OpenFile(c.String("standard-log"), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		logger.Logger = log.New(standardLogFile, "\n", log.Ldate|log.Ltime|log.Llongfile)
		sqlLogFile, err := os.OpenFile(c.String("sql-log"), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		models.DB.SetLogger(log.New(sqlLogFile, "\n", log.Ldate|log.Ltime|log.Llongfile))
		if c.Bool("dev") {
			go func() {
				log.Println(http.ListenAndServe("localhost:6060", nil))
			}()
		}
		return nil
	}
	App.Commands = []cli.Command{
		{
			Name:    "latest",
			Aliases: []string{"l"},
			Usage:   "parse latest performs",
			Action: func(c *cli.Context) error {
				job.Latest(&progress.Console{})
				return nil
			},
		},
		{
			Name:    "reindex",
			Aliases: []string{"r"},
			Usage:   "recreate elasticsearch indexes",
			Action: func(c *cli.Context) error {
				job.Reindex(&progress.Console{})
				return nil
			},
		},
		{
			Name:  "composer-dates",
			Usage: "load composer dates of birth and death",
			Action: func(c *cli.Context) error {
				job.ComposerDates(&progress.Console{})
				return nil
			},
		},
		{
			Name:  "all",
			Usage: "parse all classic-online.ru",
			Action: func(c *cli.Context) error {
				job.All(&progress.Console{})
				return nil
			},
		},
		{
			Name:  "dl",
			Usage: "dl all classic-online.ru mp3 files",
			Action: func(c *cli.Context) error {
				dl.Dl(&progress.Console{})
				return nil
			},
		},
		{
			Name:    "fragments",
			Aliases: []string{"frags"},
			Usage:   "Load fragments from files",
			Action: func(c *cli.Context) error {
				fr.Load("/data/c2/000/000/000/000/000", ".mp3.txt", &progress.Console{})
				return nil
			},
		},
		{
			Name:  "ic",
			Usage: "update urls from intoclassics.net",
			Action: func(c *cli.Context) error {
				ic.UpdateUrls(&progress.Console{})
				return nil
			},
		},
		{
			Name:  "relatest",
			Usage: "update likes and comments for range",
			Action: func(c *cli.Context) error {
				job.RangeBack(100, 200)(&progress.Console{})
				return nil
			},
		},
		{
			Name:  "heads",
			Usage: "fetch content_length for all mp3 from classic-online.ru",
			Action: func(c *cli.Context) error {
				job.Heads(&progress.Console{})
				return nil
			},
		},
		{
			Name:  "compile-assets",
			Usage: "compile admin and login page",
			Action: func(c *cli.Context) error {
				Prepare()
				assets.AssetFS.Compile()
				return nil
			},
		},
		{
			Name:  "serve",
			Usage: "start server",
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:  "gin-debug",
					Usage: "more gin-router logs",
				},
				cli.BoolFlag{
					Name:  "daemon",
					Usage: "run as a daemon",
				},
				cli.StringFlag{
					Name:  "pid-file",
					Usage: "path to pid file",
					Value: "./tmp/pid",
				},
				cli.StringFlag{
					Name:  "log-file",
					Usage: "path to daemon log file",
					Value: "./log/daemon.log",
				},
				cli.StringFlag{
					Name:  "work-dir",
					Usage: "working directory",
					Value: "./",
				},
				cli.StringFlag{
					Name:        "sock",
					Usage:       "socket file",
					Value:       "./tmp/sock",
					Destination: &Sock,
				},
			},
			Action: func(c *cli.Context) error {
				Serve = true
				if c.Bool("daemon") {
					Daemon = &daemon.Context{
						PidFileName: c.String("pid-file"),
						PidFilePerm: 0644,
						LogFileName: c.String("log-file"),
						LogFilePerm: 0640,
						WorkDir:     c.String("work-dir"),
						Umask:       027,
					}
				}
				return nil
			},
		},
	}

	sort.Sort(cli.FlagsByName(App.Flags))
	sort.Sort(cli.CommandsByName(App.Commands))
}

func Prepare() {
	auth.Init()
	admin.Init()
	workers.Init()
}

func Run() {
	App.Run(os.Args)
}

package parser

import (
	"bytes"
	"compress/gzip"
	"errors"

	//"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/pipocavsobake/goclon/app/models"
	"golang.org/x/text/encoding/charmap"
	resty "gopkg.in/resty.v1"
	yaml "gopkg.in/yaml.v2"
	//"time"
)

func gUnzipData(data []byte) (resData []byte, err error) {
	b := bytes.NewBuffer(data)

	var r io.Reader
	r, err = gzip.NewReader(b)
	if err != nil {
		panic(err)
	}

	var resB bytes.Buffer
	_, err = resB.ReadFrom(r)
	if err != nil {
		return
	}

	resData = resB.Bytes()

	return
}

var Headers map[string]string

func init() {
	headersPath := "./config/headers.yml"
	pwd, err := filepath.Abs("./")
	if filepath.Base(pwd) == "parser" {
		headersPath = "../config/headers.yml"
	}
	dat, err := ioutil.ReadFile(headersPath)
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal([]byte(dat), &Headers)
	if err != nil {
		panic(err)
	}
}

func GetPage(client *resty.Client, urlOrPath string) ([]byte, error) {
	u, err := url.Parse(urlOrPath)
	if err != nil {
		return []byte{}, err
	}
	base, err := url.Parse("https://classic-online.ru")
	if err != nil {
		return []byte{}, err
	}
	uri := base.ResolveReference(u).String()
	resp, err := client.R().Get(uri)
	if err != nil {
		return []byte{}, err
	}
	bytes := resp.Body()
	//gz := resp.Body()
	//bytes, err := gUnzipData(gz)
	//if err != nil {
	//return []byte{}, err
	//}
	dec := charmap.Windows1251.NewDecoder()
	return dec.Bytes(bytes)
}

func GetDoc(client *resty.Client, url string) (ret *goquery.Document, err error) {
	data, err := GetPage(client, url)
	if err != nil {
		return
	}
	//t := time.Now().Unix()
	//ioutil.WriteFile(fmt.Sprintf("%d.html", t), data, 0644)
	return goquery.NewDocumentFromReader(bytes.NewReader(data))
}

func NewClient() *resty.Client {
	client := resty.New()
	client.SetHeaders(Headers)
	return client
}

func BiggestID(client *resty.Client) (id int, err error) {
	doc, err := GetDoc(client, "/")
	if err != nil {
		return
	}
	doc.Find("audio[id]").Each(func(i int, s *goquery.Selection) {
		var pid string
		var ipid int
		pid, _ = s.Attr("id")
		pid = strings.TrimPrefix(pid, "player_")
		ipid, err = strconv.Atoi(pid)
		if err != nil {
			return
		}
		if id < ipid {
			id = ipid
		}
	})
	return
}

func Page(client *resty.Client, uri string) error {
	doc, err := GetDoc(client, uri)
	if err != nil {
		return err
	}
	c := composer(doc)
	piece := piece(doc, c)
	group := group(doc)
	perform := perform(doc, piece, group)
	comments(client, doc, perform)
	perform.Reindex()
	return err
}

func ComposerPage(client *resty.Client, c *models.Composer) error {
	doc, err := GetDoc(client, c.ClassicOnlineId)
	if err != nil {
		return errors.New(err.Error() + " get_doc " + c.ClassicOnlineId)
	}
	s := doc.Find(`#vse > div > div > div > div > table > tbody > tr > td.top > span > span`).First()
	if s == nil {
		return errors.New("Не получилось достать дату " + c.ClassicOnlineId)
	}
	b, d, _ := ComposerDateText(s.Text())
	if b != 0 {
		c.BirthYear = &b
	}
	if d != 0 {
		c.DeathYear = &d
	}
	models.DB.Save(c)

	return nil
}

func ComposerDateText(s string) (b, d int64, errs error) {
	s = strings.Trim(s, "()")
	var err error
	var msgs string
	if strings.Contains(s, "–") {
		bd := strings.Split(s, "–")
		b, err = strconv.ParseInt(bd[0], 10, 64)
		if err != nil {
			msgs = "split[0]: " + err.Error()
		}
		d, err = strconv.ParseInt(bd[1], 10, 64)
		if err != nil {
			msgs += "split[1] " + err.Error()
		}
		errs = errors.New(msgs)
		return
	} else if strings.Contains(s, "Род. ") {
		bs := strings.TrimPrefix(s, "Род. ")
		b, errs = strconv.ParseInt(bs, 10, 64)
		return
	}
	errs = errors.New("no date")
	return
}

package parser

import (
	"testing"
)

func TestComposerDateText(t *testing.T) {
	ss := []string{"(1747–1818)", "(Род. 1942)"}
	for _, s := range ss {
		b, d, err := ComposerDateText(s)
		println(s, b, d, err)
	}
}

func TestBiggestID(t *testing.T) {
	cli := NewClient()
	id, err := BiggestID(cli)
	if err != nil {
		println(err.Error())
	}
	if id == 0 {
		t.Error("id == 0")
	}
}

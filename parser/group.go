package parser

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/pipocavsobake/goclon/app/models"

	"strings"
)

type groupResult struct {
	GroupID int64
}

func group(doc *goquery.Document) (group *models.Group) {
	s := doc.Find(".performer_name a")
	names := make([]string, s.Length())
	urls := s.Map(func(i int, se *goquery.Selection) string {
		url, exists := se.Attr("href")
		if !exists {
			panic("Нет ссылки на исполнителя")
		}
		names[i] = se.Text()
		return url
	})
	//fmt.Println(names, urls)

	performerIDs := make([]int64, len(urls))
	sperformerIDs := make([]string, len(urls))
	performers := make([]*models.Performer, len(urls))
	for i, url := range urls {
		performer := &models.Performer{}
		models.DB.FirstOrInit(performer, models.Performer{ClassicOnlineId: url})
		performer.Name = names[i]
		models.DB.Save(performer)
		performerIDs[i] = performer.ID
		sperformerIDs[i] = fmt.Sprintf("%d", performer.ID)
		performers[i] = performer
	}
	var group_id groupResult
	models.DB.Raw(fmt.Sprintf(
		"select group_id from "+
			"(select group_id, uniq(sort(array_agg(performer_id::int))) "+
			"as performer_ids from group_performers group by group_id) "+
			"as aggs where aggs.performer_ids = ARRAY[%s]",
		strings.Join(sperformerIDs, ",")),
	).Scan(&group_id)
	//fmt.Println("\n\n\n", group_id)
	if id := group_id.GroupID; id > 0 {
		group = &models.Group{}
		models.DB.Where("id = ?", id).First(group)
		return
	}
	group = &models.Group{}
	models.DB.Create(group)
	for _, performer := range performers {
		gp := &models.GroupPerformer{}
		gp.Performer = performer
		gp.Group = group
		models.DB.Save(gp)
	}

	return
}

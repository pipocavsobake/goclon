package parser

import (
	"github.com/PuerkitoBio/goquery"
	"strings"
	"testing"
)

func TestCommentText(t *testing.T) {
	s := `<!DOCTYPE HTML><html><body><table><tr><td class="cont"><strong>Miao писал(а):</strong><div>)) Это специально написано, чтоб клавесинисты пальцы ломали? Точно, Бах был весёлым человеком!</div>Пальцы ломают на 3-4 такте, а дальше играют ногами.</td></tr></table></body></html>`
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(s))
	if err != nil {
		t.Error(err)
	}
	text := commentText(doc.Find("html").First())
	if text != "Пальцы ломают на 3-4 такте, а дальше играют ногами." {
		t.Error("Text should not be", text)
	}
	cite := commentCite(doc.Find("html").First())
	if cite != ")) Это специально написано, чтоб клавесинисты пальцы ломали? Точно, Бах был весёлым человеком!" {
		t.Error("cite should not be", cite)
	}
	citeAuthor := commentCiteAuthor(doc.Find("html").First())
	if citeAuthor != "Miao писал(а):" {
		t.Error("cite author should not be", citeAuthor)
	}
}

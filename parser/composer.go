package parser

import (
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/config/logger"
)

func composer(doc *goquery.Document) (ret *models.Composer) {
	ret = &models.Composer{}
	s := doc.Find(".composer_name a").First() //.Attr("href")
	url, exist := s.Attr("href")
	if !exist {
		h, _ := s.Html()
		println(h)
		panic("Нет ссылки на композитора")
	}
	models.DB.FirstOrInit(ret, models.Composer{ClassicOnlineId: url})
	if ret == nil {
		logger.Logger.Println("url", url)
		panic("Не получилось найти или создать композитора")
	}
	ret.Name = s.Text()
	models.DB.Save(ret)
	return
}

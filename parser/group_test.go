package parser

import (
	"testing"

	"gitlab.com/pipocavsobake/goclon/app/models"
)

func TestGroup(t *testing.T) {
	cli := NewClient()
	err := Page(cli, "https://classic-online.ru/archive/?file_id=1884")
	if err != nil {
		t.Error(err.Error())
	}
	perform := models.Perform{}
	models.PerformSearchScope().Where("classic_online_id = ?", "1884").First(&perform)
	s := perform.SearchData()
	if l := len(s.Performers); l != 1 {
		t.Error(l)
	}
}

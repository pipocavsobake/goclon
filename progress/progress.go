package progress

type ProgressHandler interface {
	Init(int)
	Add(int)
	Set(int)
	Finish()
	Fail(string)
}

package progress

import (
	"gopkg.in/cheggaaa/pb.v1"
)

type Console struct {
	bar *pb.ProgressBar
	l   int
}

func (c *Console) Init(t int) {
	c.bar = pb.StartNew(t)
}

func (c *Console) Add(i int) {
	for k := 0; k < i; k++ {
		c.bar.Increment()
	}
}

func (c *Console) Set(i int) {
	for ; c.l < i; c.l++ {
		c.bar.Increment()
	}
}

func (c *Console) Finish() {
	if c.bar == nil {
		return
	}
	c.bar.FinishPrint("The end")
}

func (c *Console) Fail(m string) {
	panic(m)
}

require 'active_record'
require 'pg'
require 'searchkick'
db_config = YAML.safe_load(File.open('../../config/database.yml'))['development']
ActiveRecord::Base.establish_connection(db_config)
ActiveRecord::Base.logger = Logger.new(STDOUT)
ActiveRecord::Base.logger.level = Logger::DEBUG
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
class SiteUser < ApplicationRecord
end
class Composer < ApplicationRecord
  has_many :pieces
end
class Piece < ApplicationRecord
  belongs_to :composer
  has_many :performs
end
class Performer < ApplicationRecord
  has_and_belongs_to_many :groups, join_table: :group_performers
end
class Group < ApplicationRecord
  has_and_belongs_to_many :performers, join_table: :group_performers
end
class User < ApplicationRecord
  has_many :performs
end
class Comment < ApplicationRecord
  belongs_to :perform
end
class Perform < ApplicationRecord
  searchkick language: 'russian'
  belongs_to :piece
  belongs_to :group
  belongs_to :user
  has_one :composer, through: :piece
  has_many :performers, through: :group
  has_many :comments
  scope :search_import, -> { preload(:user, :piece, :composer, :performers, :comments) }
  def search_data
    attributes.merge(
      user: user.name,
      piece: piece.name,
      composer: composer.name,
      performer_names: performers.map(&:name),
      comments_count: comments.size,
      comments_max: comments.map(&:likes).max,
      comments_sum: comments.map(&:likes).sum,
      summary: [composer.name, piece.name] + performers.map(&:name)
    )
  end
end

binding.irb

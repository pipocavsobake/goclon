package controllers

import (
	"fmt"

	"github.com/pkg/errors"
	"gitlab.com/pipocavsobake/goclon/app/models"
)

func PG2json(performs []*models.Perform, siteUser *models.SiteUser) ([]models.PerformApiData, error) {
	performIDS := make([]int64, len(performs))
	pieceIDs := make([]int64, len(performs))
	for i, perform := range performs {
		performIDS[i] = perform.ID
		if perform.PieceID != nil {
			pieceIDs[i] = *perform.PieceID
		}
	}
	items := make([]models.PerformApiData, len(performs))
	directories, err := getDirectoriesFor(performIDS, siteUser)
	if err != nil {
		return items, err
	}
	favorites, err := GetFavoritesFor(performIDS, siteUser)
	if err != nil {
		return items, err
	}
	mss, err := GetLastMSFor(performIDS, siteUser)
	if err != nil {
		return items, err
	}
	bests, err := GetBestPerformsFor(pieceIDs, siteUser)
	if err != nil {
		return items, errors.Wrap(err, "GetBestPerformsFor")
	}
	for i, perform := range performs {
		items[i] = (perform.SearchDataWithID())
		items[i].IsFavorite = favorites[i]
		items[i].LastMS = mss[i]
		items[i].Directories = make([]models.DirectoryApiData, 0)
		items[i].BestPerforms = bests[i]
		for _, d := range directories {
			if items[i].ID != d.performID {
				continue
			}
			items[i].Directories = append(items[i].Directories, models.DirectoryApiData{Name: d.Name, ID: d.ID})
		}
	}
	return items, nil
}

func GetFavoritesFor(performIDS []int64, cu *models.SiteUser) (ret []bool, err error) {
	ret = make([]bool, len(performIDS))
	if cu == nil || len(performIDS) == 0 {
		return
	}
	sups := []*models.SiteUserPerform{}
	if err = models.DB.Where("site_user_id = ? and perform_id in (?)", cu.ID, performIDS).Find(&sups).Error; err != nil {
		return
	}
	if len(sups) == 0 {
		return
	}
	for _, sup := range sups {
		for i, performID := range performIDS {
			if sup.PerformID != nil && performID == *sup.PerformID {
				ret[i] = sup.Like
			}
		}
	}

	return
}

func GetLastMSFor(performIDS []int64, cu *models.SiteUser) (ret []int64, err error) {
	ret = make([]int64, len(performIDS))
	if cu == nil || len(performIDS) == 0 {
		return
	}
	events := []*models.Event{}
	if err = models.DB.Where("site_user_id = ? and kind = 'PAUSE' and (perform_id, client_time) in (select perform_id, MAX(client_time) from events where site_user_id = ? and kind = 'PAUSE' and perform_id in (?) group by perform_id)", cu.ID, cu.ID, performIDS).Find(&events).Error; err != nil || len(events) == 0 {
		return
	}
	for _, event := range events {
		for i, performID := range performIDS {
			if event.PerformID != nil && performID == *event.PerformID {
				ret[i] = event.Value
			}
		}
	}
	return
}

func GetBestPerformsFor(pieceIDs []int64, cu *models.SiteUser) (ret [][]*models.PerformApiData, err error) {
	ret = make([][]*models.PerformApiData, len(pieceIDs))
	if cu == nil || len(pieceIDs) == 0 {
		return
	}
	fights := make([]models.Fight, 0)
	if err = models.DB.Model(models.Fight{}).Preload("FightPerforms.Perform.Piece.Composer").Preload("FightPerforms.Perform.Group.GroupPerformers.Performer").Where("site_user_id = ? AND piece_id IN (?)", cu.ID, pieceIDs).Find(&fights).Error; err != nil {
		return
	}
	fightsMap := make(map[int64]models.Fight)
	for _, fight := range fights {
		if fight.PieceID == nil {
			err = errors.New(fmt.Sprintf("fight(id=%v).PieceID == nil", fight.ID))
			return
		}
		fightsMap[*fight.PieceID] = fight
	}
	for i, pieceID := range pieceIDs {
		if fight, exists := fightsMap[pieceID]; exists {
			if performs, positions, err := fight.BestPerforms(); err != nil {
				err1 := errors.Wrap(err, "fight.BestPerforms")
				return ret, err1
			} else {
				ret[i] = make([]*models.PerformApiData, len(performs))
				for j, perform := range performs {
					data := perform.SearchDataWithID()
					data.Position = &positions[j]
					ret[i][j] = &data
				}
			}
		}
	}
	return
}

type DirectoryItem struct {
	Name      string
	ID        int64
	performID int64
}

func getDirectoriesFor(performIDS []int64, siteUser *models.SiteUser) ([]DirectoryItem, error) {
	directories := []DirectoryItem{}
	if siteUser == nil {
		return directories, nil
	}

	rows, err := models.DB.Raw("SELECT directories.name, directories.id, perform_directories.perform_id FROM directories JOIN perform_directories on perform_directories.directory_id = directories.id WHERE perform_id IN (?) AND directories.site_user_id = ?", performIDS, siteUser.ID).Rows()
	if err != nil {
		return directories, err
	}
	defer rows.Close()
	for rows.Next() {
		d := DirectoryItem{}
		rows.Scan(&d.Name, &d.ID, &d.performID)
		directories = append(directories, d)
	}
	return directories, nil
}

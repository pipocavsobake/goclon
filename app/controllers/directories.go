package controllers

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"

	"errors"
	"strconv"
)

// @Summary List of directories
// @Accept  json
// @Produce json
// @Param limit query int false "Limit" default(20)
// @Param offset query int false "Offset" default(0)
// @Success 200 {object} models.DirectoryApiData
// @Router /api/directories [get]
func DirectoriesIndex(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	params := GetSearchQueryParams(ctx)
	directories := []*models.Directory{}
	scope := models.DB.Model(&models.Directory{}).Where("site_user_id = ?", cu.ID).Order("name")
	total := 0
	if err := scope.Count(&total).Error; err != nil {
		ctx.AbortWithError(500, err)
		return
	}

	if err := scope.Limit(params.Size).Offset(params.From).Find(&directories).Error; err != nil {
		ctx.AbortWithError(500, err)
		return
	}

	data := make([]models.DirectoryApiData, len(directories))
	for i, d := range directories {
		data[i] = d.ApiData()
	}

	ctx.JSON(200, gin.H{
		"total": total,
		"data":  data,
	})

}

type directoryCreateParams struct {
	Name string `json:"name"`
}

// @Summary Create Directory
// @Description create directory by name for current user
// @Accept json
// @Produce json
// @Param name body string true "any characters" maxlength(256)
// @Success 200 {object} models.DirectoryApiData
// @Router /api/directories [post]
func DirectoriesCreate(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	data := directoryCreateParams{}
	if err := ctx.BindJSON(&data); err != nil {
		ctx.AbortWithError(422, err)
		return
	}
	dir := models.Directory{SiteUserID: &cu.ID, Name: data.Name}
	if err := models.DB.Save(&dir).Error; err != nil {
		ctx.AbortWithError(422, err)
		return
	}
	ctx.JSON(200, dir.ApiData())
}

// @Summary delete a directory
// @Param directory_id path int true "Id of the directory"
// @Success 204 {object} models.EmptyResult
// @Router /api/directories/{directory_id} [delete]
func DirectoriesDelete(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.AbortWithError(404, errors.New("Cannot find directory with id "+ctx.Param("id")))
		return
	}
	directory := models.Directory{}
	if err = models.DB.Where("id = ?", id).First(&directory).Error; err != nil || directory.ID == 0 {
		ctx.AbortWithError(404, errors.New("Cannot find directory with id "+ctx.Param("id")))
		return
	}
	if err = models.DB.Delete(&directory).Error; err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	ctx.AbortWithStatus(204)
}

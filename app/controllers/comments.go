package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"
	"net/http"
)

type updateParams struct {
	Approved []int64 `json:"approved"`
	Declined []int64 `json:"declined"`
}

func CommentsUpdate(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		return
	}
	var data updateParams
	err := ctx.BindJSON(&data)
	if err != nil {
		ctx.AbortWithError(422, err)
		return
	}
	tx := models.DB.Begin()
	if tx.Error != nil {
		ctx.AbortWithError(422, err)
		return
	}
	for _, comment_id := range data.Approved {
		err = tx.Exec("INSERT INTO marks (site_user_id, approval, comment_id) VALUES (?,?,?) ON CONFLICT (site_user_id, comment_id) DO UPDATE SET approval = ?", cu.ID, true, comment_id, true).Error
		if err != nil {
			ctx.AbortWithError(422, err)
			return
		}
	}
	for _, comment_id := range data.Declined {
		err = tx.Exec("INSERT INTO marks (site_user_id, approval, comment_id) VALUES (?,?,?) ON CONFLICT (site_user_id, comment_id) DO UPDATE SET approval = ?", cu.ID, false, comment_id, false).Error
		if err != nil {
			ctx.AbortWithError(422, err)
			return
		}
	}
	tx.Commit()

	ctx.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"approved": data.Approved,
		"declined": data.Declined,
	})
}

func GetPaginationParams(ctx *gin.Context) (page int, page_size int) {
	page = getIntQueryParam(ctx, "page", "1", 1)
	if page < 1 {
		page = 1
	}
	page_size = getIntQueryParam(ctx, "page_size", "100", 100)
	if page_size < 0 || page_size > MAX_LIMIT {
		page_size = 100
	}
	return
}

func CommentsIndex(ctx *gin.Context) {
	user := concerns.AuthJSON(ctx)
	if user == nil {
		return
	}
	comments := []*models.Comment{}
	page, page_size := GetPaginationParams(ctx)
	err := models.DB.Model(&models.Comment{}).Where("id NOT IN (SELECT comment_id FROM marks WHERE site_user_id = ?)", user.ID).Order("id asc").Limit(page_size).Offset((page - 1) * page_size).Find(&comments).Error
	if err != nil {
		ctx.AbortWithError(422, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data":   comments,
	})
}

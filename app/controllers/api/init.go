package api

import (
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"
)

type ApiError struct {
	Error string `json:"error"`
}

type Paginatable interface {
	GetPage() int64
	GetPerPage() int64
}

func Paginate(params Paginatable, scope *gorm.DB) *gorm.DB {
	limit := params.GetPerPage()
	if limit <= 0 || limit >= 100 {
		limit = 20
	}
	page := params.GetPage()
	if page <= 0 {
		page = 1
	}
	offset := (page - 1) * limit
	return scope.Offset(offset).Limit(limit)
}

var Index map[string]func(ctx *gin.Context)

func init() {
	//spew.Dump(models.DB.NewScope(&models.Article{}).GetStructFields()[0])
	Index = generateApiIndexMap(
		&models.Article{},
		&models.Comment{},
		&models.Composer{},
		&models.Directory{},
		&models.Event{},
		&models.Fight{},
		&models.Fragment{},
		&models.Job{},
		&models.Mark{},
		&models.Perform{},
		&models.Performer{},
		&models.Piece{},
		&models.Post{},
		&models.SiteUser{},
		&models.Source{},
		&models.Text{},
		&models.User{},
		&models.Preference{},
	)
}

func doString(db *gorm.DB, key string, values []string) *gorm.DB {
	if strings.HasSuffix(key, "_equals") {
		field := strings.TrimSuffix(key, "_equals")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), values[0])
	} else if strings.HasSuffix(key, "_eq") {
		field := strings.TrimSuffix(key, "_eq")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), values[0])
	} else if strings.HasSuffix(key, "_starts_with") {
		field := strings.TrimSuffix(key, "_starts_with")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), values[0]+"%")
	} else if strings.HasSuffix(key, "_ends_with") {
		field := strings.TrimSuffix(key, "_ends_with")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), "%"+values[0])
	} else if strings.HasSuffix(key, "_contains") {
		field := strings.TrimSuffix(key, "_contains")
		return db.Where(fmt.Sprintf("%v ILIKE ?", field), "%"+values[0]+"%")
	}
	return db
}

func doInt(db *gorm.DB, key string, values []string) *gorm.DB {
	value, err := strconv.ParseInt(values[0], 10, 64)
	if err != nil {
		return db.Where("1 = 0")
	}
	if strings.HasSuffix(key, "_eq") {
		field := strings.TrimSuffix(key, "_eq")
		return db.Where(fmt.Sprintf("%v = ?", field), value)
	} else if strings.HasSuffix(key, "_equals") {
		field := strings.TrimSuffix(key, "_equals")
		return db.Where(fmt.Sprintf("%v = ?", field), value)
	} else if strings.HasSuffix(key, "_greater_than") {
		field := strings.TrimSuffix(key, "_greater_than")
		return db.Where(fmt.Sprintf("%v > ?", field), value)
	} else if strings.HasSuffix(key, "_less_than") {
		field := strings.TrimSuffix(key, "_less_than")
		return db.Where(fmt.Sprintf("%v < ?", field), value)
	}
	return db
}

func doTime(db *gorm.DB, key string, values []string) *gorm.DB {
	value, err := time.Parse(time.RFC3339, strings.ToUpper(values[0]))
	if err != nil {
		//spew.Dump(err)
		return db.Where("1 = 0")
	}
	if strings.HasSuffix(key, "_gteq_datetime") {
		field := strings.TrimSuffix(key, "_gteq_datetime")
		return db.Where(fmt.Sprintf("%v > ?", field), value)
	} else if strings.HasSuffix(key, "_lteq_datetime") {
		field := strings.TrimSuffix(key, "_lteq_datetime")
		return db.Where(fmt.Sprintf("%v < ?", field), value)
	}
	return db
}

func sort(db *gorm.DB, r *http.Request) *gorm.DB {
	sorts, ok := r.URL.Query()["sort"]
	if !ok {
		sorts, ok = r.URL.Query()["sort[]"]
		if !ok {
			return db
		}
	}
	s := db
	for _, sort := range sorts {
		switch sort[0] {
		case '+':
			sort = sort[1:]
			break
		case '-':
			sort = sort[1:] + " DESC"
			break
		}
		s = s.Order(sort)
	}
	return s
}

func paginate(db *gorm.DB, r *http.Request) *gorm.DB {
	pageS := r.URL.Query().Get("page")
	if pageS == "" {
		pageS = "1"
	}
	perPageS := r.URL.Query().Get("per_page")
	if perPageS == "" {
		perPageS = "20"
	}
	page, err := strconv.ParseInt(pageS, 10, 64)
	if err != nil {
		page = 1
	}
	perPage, err := strconv.ParseInt(perPageS, 10, 64)
	if err != nil {
		perPage = 20
	}
	if perPage <= 0 || perPage >= 50 {
		perPage = 20
	}
	if page <= 0 {
		page = 1
	}
	return db.Limit(perPage).Offset((page - 1) * perPage)
}

func getType(t reflect.Type) string {
	switch t.Kind() {
	case reflect.Int64:
		return "int"
	case reflect.String:
		return "string"
	case reflect.Ptr:
		return getType(t.Elem())
	case reflect.Struct:
		if t.PkgPath() == "time" && t.Name() == "Time" {
			return "time"
		}
	}
	return ""
}

func getPreloadType(t reflect.Type) string {
	switch t.Kind() {
	case reflect.Slice:
		return getPreloadType(t.Elem())
	case reflect.Ptr:
		return getPreloadType(t.Elem())
	default:
		return t.PkgPath() + "." + t.Name()
	}
}

func bigrams(source, delimiter string) [][2]string {
	ret := [][2]string{}
	ar := strings.Split(source, delimiter)
	for i := 1; i < len(ar); i++ {
		ret = append(ret, [2]string{ar[i-1], ar[i]})
	}
	return ret
}

func generateApiIndexMap(structs ...interface{}) map[string]func(ctx *gin.Context) {
	ret := map[string]func(ctx *gin.Context){}
	skips := map[string]struct{}{}
	preloadType := map[string]string{}
	for _, modelT := range structs {
		model := modelT
		elem := reflect.TypeOf(model).Elem()
		modelName := elem.PkgPath() + "." + elem.Name()
		scope := models.DB.NewScope(model)
		types := map[string]string{}
		//spew.Dump(reflect.TypeOf(model).Elem().Name())
		//preloads := map[string]struct{}{}
		for _, field := range scope.GetStructFields() {
			fieldKey := modelName + "::" + field.Name
			settings := field.Struct.Tag.Get("api")
			//spew.Dump(fieldKey, settings)
			if settings == "-" {
				skips[fieldKey] = struct{}{}
				continue
			}
			if field.Relationship != nil {
				types[field.Name] = "preload"
				preloadType[fieldKey] = getPreloadType(field.Struct.Type)
				continue
			}
			types[field.DBName] = getType(field.Struct.Type)
		}
		//spew.Dump(preloadType)
		ret["/"+scope.TableName()] = func(ctx *gin.Context) {
			cu := concerns.Auth(ctx)
			q := ctx.Request.URL.Query()
			scope := models.DB.Model(model)
			if _, ok := reflect.TypeOf(model).MethodByName("Authorize"); ok {
				reflect.ValueOf(model).MethodByName("Authorize").Call([]reflect.Value{reflect.ValueOf(scope), reflect.ValueOf(cu)})
			}
			for _, key := range []string{"includes", "includes[]"} {
				if values, ok := q[key]; ok {
					for _, value := range values {
						lastModel := modelName
						ar := strings.Split(value, ".")
						schema := []string{}
						for _, fn := range ar {
							key := lastModel + "::" + fn
							//spew.Dump(skips)
							if _, skip := skips[key]; skip {
								break
							}
							schema = append(schema, fn)
							lastModel = preloadType[key]
						}
						if len(schema) != 0 {
							scope = scope.Preload(strings.Join(schema, "."))
						}
					}
				}
			}
			for key, values := range q {
				for field, kind := range types {
					field = strings.TrimSuffix(field, "[]")
					if strings.HasPrefix(key, field) {
						switch kind {
						case "string":
							scope = doString(scope, key, values)
							break
						case "int":
							scope = doInt(scope, key, values)
							break
						case "time":
							scope = doTime(scope, key, values)
							break
						}
					}
				}
			}
			c := 0
			//array := reflect.MakeSlice(reflect.SliceOf(reflect.TypeOf(model)), 0, 20)
			//array := reflect.ValueOf(model).MethodByName("MakeSlice").Call([]reflect.Value{})
			if err := scope.Count(&c).Error; err != nil {
				ctx.JSON(http.StatusBadRequest, err.Error())
				return
			}
			scope = sort(scope, ctx.Request)
			scope = paginate(scope, ctx.Request)
			//spew.Dump(model)
			//spew.Dump(reflect.ValueOf(model).MethodByName("MakeResponse"))
			reflect.ValueOf(model).MethodByName("MakeResponse").Call([]reflect.Value{
				reflect.ValueOf(c), reflect.ValueOf(scope), reflect.ValueOf(ctx),
			})
		}

	}
	return ret
}

package directories

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"

	"gitlab.com/pipocavsobake/goclon/app/controllers"
	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"

	"strconv"
)

func findUserAndDir(ctx *gin.Context) (cu *models.SiteUser, dir *models.Directory, err error) {
	cu = concerns.AuthJSON(ctx)
	if cu == nil {
		err = errors.New("Anauthorized")
		ctx.AbortWithError(401, err)
		return
	}
	id, err := strconv.Atoi(ctx.Param("directory_id"))
	if err != nil {
		err = errors.New("Cannot find directory with id " + ctx.Param("directory_id"))
		ctx.AbortWithError(404, err)
		return
	}
	dir = &models.Directory{}
	if err = models.DB.Model(&models.Directory{}).Where("id = ?", id).First(&dir).Error; err != nil || dir.ID == 0 {
		err = errors.New("Cannot find directory with id " + ctx.Param("directory_id"))
		ctx.AbortWithError(404, err)
		return
	}
	return
}

// @Summary List of performs in a directory
// @Accept json
// @Product json
// @Param directory_id path int true "ID of the directory"
// @Param limit query int false "Limit" default(20)
// @Param offset query int false "Offset" default(0)
// @Success 200 {object} models.PerformResult
// @Router /api/directories/{directory_id}/performs [get]
func MemberPerformsIndex(ctx *gin.Context) {
	cu, dir, err := findUserAndDir(ctx)
	if err != nil {
		return
	}

	total := 0
	params := controllers.GetSearchQueryParams(ctx)
	scope := models.PerformSearchScope().Where("id IN (SELECT perform_id from perform_directories WHERE directory_id = ?)", dir.ID)
	if err = scope.Count(&total).Error; err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	performs := []*models.Perform{}
	if err = scope.Offset(params.From).Limit(params.Size).Find(&performs).Error; err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	data, err := controllers.PG2json(performs, cu)
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, gin.H{
		"total": total,
		"data":  data,
	})
}

func findPerform(ctx *gin.Context) (perform *models.Perform, err error) {
	id, err := strconv.Atoi(ctx.Param("perform_id"))
	if err != nil {
		err = errors.New("Cannot find perform_directory with perform_id " + ctx.Param("perform_id"))
		ctx.AbortWithError(404, err)
		return
	}
	perform = &models.Perform{}

	if err = models.PerformSearchScope().Where("id = ?", id).First(perform).Error; err != nil || perform.ID == 0 {
		err = errors.New("Cannot find perform_directory with perform_id " + ctx.Param("perform_id"))
		ctx.AbortWithError(404, err)
		return
	}
	return
}

// @Summary Remove perform from the directory
// @Accept json
// @Product json
// @Param directory_id path int true "ID of the directory"
// @Param perform_id path int true "ID of the perform"
// @Success 200 {object} models.PerformApiData
// @Router /api/directory/{directory_id}/performs/{perform_id} [delete]
func MemberPerformsDelete(ctx *gin.Context) {
	cu, dir, err := findUserAndDir(ctx)
	if err != nil {
		return
	}
	perform, err := findPerform(ctx)
	if err != nil {
		return
	}

	record := models.PerformDirectory{}

	if err = models.DB.Where("perform_id = ? and directory_id = ?", perform.ID, dir.ID).First(&record).Error; err != nil || record.ID == 0 {
		err = errors.New("Cannot find perform_directory with perform_id " + ctx.Param("perform_id"))
		ctx.AbortWithError(404, err)
		return
	}

	if err = models.DB.Delete(&record).Error; err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	data, err := controllers.PG2json([]*models.Perform{perform}, cu)
	if err != nil {
		ctx.AbortWithError(500, errors.Wrap(err, "pg2json"))
		return
	}
	if len(data) != 1 {
		ctx.AbortWithError(500, errors.New("len(data)!=1"))
		return
	}
	ctx.JSON(200, data[0])
}

// @Summary Add a perform to the directory
// @Accept json
// @Product json
// @Param directory_id path int true "ID of the directory"
// @Param perform_id path int true "ID of the perform"
// @Success 200 {object} models.PerformApiData
// @Router /api/directory/{directory_id}/performs/{perform_id} [post]
func MemberPerformsCreate(ctx *gin.Context) {
	cu, dir, err := findUserAndDir(ctx)
	if err != nil {
		return
	}
	perform, err := findPerform(ctx)
	if err != nil {
		return
	}
	record := models.PerformDirectory{PerformID: &perform.ID, DirectoryID: &dir.ID}
	if err := models.DB.Save(&record).Error; err != nil {
		ctx.AbortWithError(422, err)
		return
	}
	data, err := controllers.PG2json([]*models.Perform{perform}, cu)
	if err != nil {
		ctx.AbortWithError(500, errors.Wrap(err, "pg2json"))
		return
	}
	if len(data) != 1 {
		ctx.AbortWithError(500, errors.New("len(data)!=1"))
		return
	}
	ctx.JSON(200, data[0])
}

package controllers

import (
	"context"
	"sort"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	elastic "github.com/olivere/elastic/v7"
	"github.com/pkg/errors"
	"gitlab.com/pipocavsobake/goclon/app/models"
)

const (
	MAX_LIMIT = 1000
)

type SearchQueryParams struct {
	Q                                string
	Size, From                       int
	Order                            string
	ComposerID, PerformerID, PieceID int
	HasAudio, Desc                   bool
	LikesGTE, CommentsCountGTE       int
}

func (params *SearchQueryParams) Subs() {
	if params.Q == "" {
		return
	}
	subs := []*models.SearchSub{}
	models.DB.Find(&subs)
	for _, sub := range subs {
		params.Q = strings.Replace(params.Q, sub.From, sub.To, -1)
	}
}

func GetSearchQueryParams(ctx *gin.Context) (params SearchQueryParams) {
	params.Q = ctx.DefaultQuery("q", "*")
	params.Size = getIntQueryParam(ctx, "limit", "20", 20)
	if params.Size < 0 || params.Size > MAX_LIMIT {
		params.Size = 20
	}
	params.From = getIntQueryParam(ctx, "offset", "0", 0)
	params.Order = ctx.DefaultQuery("order", "")
	if desc := ctx.DefaultQuery("desc", ""); desc == "true" || desc == "1" {
		params.Desc = true
	}
	params.ComposerID = getIntQueryParam(ctx, "composer_id", "0", 0)
	params.PerformerID = getIntQueryParam(ctx, "performer_id", "0", 0)
	params.PieceID = getIntQueryParam(ctx, "piece_id", "0", 0)
	params.LikesGTE = getIntQueryParam(ctx, "likes_gte", "0", 0)
	params.CommentsCountGTE = getIntQueryParam(ctx, "comments_count_gte", "0", 0)
	if ha := ctx.DefaultQuery("has_audio", ""); ha == "true" || ha == "1" {
		params.HasAudio = true
	}
	params.Subs()
	return
}

func (params SearchQueryParams) SearchQuery() *elastic.MatchQuery {
	return elastic.NewMatchQuery("summary", params.Q).Fuzziness("AUTO").Operator("AND")
}

func (params SearchQueryParams) SearchService(q elastic.Query) *elastic.SearchService {
	return models.ES.Search().Index(models.PERFORM_INDEX).Query(q).From(params.From).Size(params.Size)
}

func (params SearchQueryParams) findPerforms(query elastic.Query, cu *models.SiteUser) (total int64, items []models.PerformApiData, err error) {
	service := params.SearchService(query)
	if params.Order == "date.keyword" || params.Order == "upload_date" {
		service = service.Sort(params.Order, !params.Desc)
	}
	res, err := service.
		Do(context.Background())
	if err != nil {
		return 0, nil, err
	}
	total = res.Hits.TotalHits.Value
	ids := make([]int64, len(res.Hits.Hits))
	for i, hit := range res.Hits.Hits {
		var err error
		ids[i], err = strconv.ParseInt(hit.Id, 10, 64)
		if err != nil {
			return 0, nil, err
		}
	}
	idsMap := map[int64]int{}
	for i, id := range ids {
		idsMap[id] = i
	}
	performs := []*models.Perform{}
	if err := models.PerformSearchScope().Where("id IN (?)", ids).Find(&performs).Error; err != nil {
		return 0, nil, errors.Wrap(err, "load performs from PG")
	}
	sort.SliceStable(performs, func(i, j int) bool {
		l, _ := idsMap[performs[i].ID]
		r, _ := idsMap[performs[j].ID]
		return l < r
	})
	if data, err := PG2json(performs, cu); err != nil {
		return 0, nil, errors.Wrap(err, "PG2json")
	} else {
		return total, data, nil
	}
}
func getIntQueryParam(ctx *gin.Context, key string, defaultString string, defaultInt int) int {
	s := ctx.DefaultQuery(key, defaultString)
	i, err := strconv.Atoi(s)
	if err != nil || s == defaultString || i < 0 {
		return defaultInt
	}
	return i
}

package concerns

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pipocavsobake/goclon/app/models"
	auth "gitlab.com/pipocavsobake/goclon/config/siteauth"
	"net/http"
)

func Auth(c *gin.Context) *models.SiteUser {
	us := auth.Auth.GetCurrentUser(c.Request)
	switch u := us.(type) {
	case nil:
		return nil
	case *models.SiteUser:
		return u
	default:
		panic("bad return from current_user")
	}
}

func AuthJSON(c *gin.Context) *models.SiteUser {
	us := Auth(c)
	if us == nil {
		c.JSON(http.StatusForbidden, gin.H{"error": "Access Denied"})
		return nil
	}

	return us
}

package controllers

import (
	"html/template"
	"net/http"
	"os"

	"github.com/go-webpack/webpack"
	"github.com/qor/render"
	"gitlab.com/pipocavsobake/goclon/assets"
)

var Render *render.Render

func init() {
	viewPath := os.Getenv("VIEW_PATH")
	if viewPath == "" {
		viewPath = "app/views"
	}
	fm := func(*render.Render, *http.Request, http.ResponseWriter) template.FuncMap {
		return template.FuncMap{
			"asset": webpack.AssetHelper,
			//"json":  json.Marshal,
		}
	}
	Render = render.New(&render.Config{
		ViewPaths:     []string{viewPath},
		DefaultLayout: "base",
		FuncMapMaker:  fm,
	})

	Render.SetAssetFS(assets.AssetFS)
}

package controllers

import (
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"
)

// @Summary Performs Show
// @Description get performs
// @Accept json
// @Produce json
// @Param perform_id path int true "Id of the perfom"
// @Param method path string true "select method" Enums(like,view)
// @Success 200 {object} models.PerformResult
// @Failure 403 {string} string "Access Denied"
// @Router /api/performs/{perform_id}/{method} [put]
func SiteUserPerformsUpdate(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Unauthorized"))
		return
	}
	performID, err := strconv.Atoi(ctx.Param("perform_id"))
	if err != nil {
		ctx.AbortWithError(404, err)
		return
	}
	perf := models.Perform{}
	models.DB.Where("id = ?", performID).First(&perf)
	if perf.ID == 0 {
		ctx.AbortWithError(404, errors.New("Cannot find Perform"))
		return
	}
	tx := models.DB.Begin()
	sup := models.SiteUserPerform{}
	tx.FirstOrCreate(&sup, models.SiteUserPerform{PerformID: &perf.ID, SiteUserID: &cu.ID})

	if ctx.Param("method") == "view" {
		err := tx.Exec("UPDATE site_user_performs SET count=count+1 WHERE id = ?", sup.ID).Error
		if err != nil {
			println(err.Error())
			tx.Rollback()
			ctx.AbortWithError(422, err)
			return
		}
	} else if ctx.Param("method") == "like" {
		sup.Like = true
		tx.Save(&sup)
	} else {
		tx.Rollback()
		ctx.AbortWithError(404, errors.New("Unknown method"))
		return
	}

	tx.Commit()
}

package controllers

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	elastic "github.com/olivere/elastic/v7"
	"github.com/pkg/errors"

	valid "github.com/asaskevich/govalidator"
	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"
)

func pErr(err error) {
	if err != nil {
		panic(err)
	}
}

// @Summary Index Performs
// @Description get list of performs
// @Accept json
// @Produce json
// @Param likes_gte query int false "minimum number of likes"
// @Param comments_gte query int false "minimum number of comments"
// @Param has_audio query bool false "must have audio flag"
// @Param piece_id query int false "piece id"
// @Success 200 {object} models.PerformResult
// @Router /api/performs [get]
func PerformsIndex(ctx *gin.Context) {
	cu := concerns.Auth(ctx)
	params := GetSearchQueryParams(ctx)
	q := elastic.NewBoolQuery()
	if params.LikesGTE > 0 {
		q.Must(elastic.NewRangeQuery("likes").Gte(params.LikesGTE))
	}
	if params.CommentsCountGTE > 0 {
		q.Must(elastic.NewRangeQuery("comments_count").Gte(params.CommentsCountGTE))
	}

	if params.HasAudio {
		q.Must(elastic.NewTermQuery("has_audio", true))
	}

	if params.PieceID > 0 {
		q.Must(elastic.NewTermQuery("piece_id", params.PieceID))
	}
	total, items, err := params.findPerforms(q, cu)
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, gin.H{
		"total": total,
		"data":  items,
	})

}

func PerformsSuggests(ctx *gin.Context) {
	params := GetSearchQueryParams(ctx)
	query := params.SearchQuery()
	service := params.SearchService(query)
	//suggester := elastic.NewCompletionSuggester("summary").Field("summary")
	//service = service.Suggester(suggester)
	res, err := service.Do(context.Background())
	if err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error(), 0})
		return
	}
	suggests := make([]string, len(res.Hits.Hits))
	for i, hit := range res.Hits.Hits {
		data := models.PerformSearchData{}
		if err := json.Unmarshal(hit.Source, &data); err != nil {
			ctx.JSON(http.StatusBadRequest, ApiError{err.Error(), 0})
			return
		}
		suggests[i] = strings.Join(data.Summary, " ")
	}

	ctx.JSON(http.StatusOK, suggests)
}

func PerformsApiSearch(ctx *gin.Context) {
	cu := concerns.Auth(ctx)
	params := GetSearchQueryParams(ctx)
	query := params.SearchQuery()
	total, items, err := params.findPerforms(query, cu)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  items,
	})
}

// @Summary Search Performs
// @Description search performs by query string
// @Accept json
// @Produce json
// @Param q query string false "query string"
// @Tag performs
// @Success 200 {object} models.PerformResult
// @Router /search [get]
func PerformsSearch(ctx *gin.Context) {
	cu := concerns.Auth(ctx)
	params := GetSearchQueryParams(ctx)

	if valid.IsInt(params.Q) {
		id, err := strconv.ParseInt(params.Q, 10, 64)
		if err != nil {
			ctx.AbortWithError(500, err)
			return
		}
		scope := models.PerformSearchScope().Where("classic_online_id = ? or id = ?", params.Q, id)
		c := 0

		if scope.Count(&c); c > 0 {
			fromPG := []*models.Perform{}
			scope.Limit(params.Size).Offset(params.From).Find(&fromPG)
			data, err := PG2json(fromPG, cu)
			if err != nil {
				ctx.AbortWithError(500, errors.Wrap(err, "pg2json"))
				return
			}
			ctx.JSON(200, gin.H{
				"total": c,
				"data":  data,
			})
			return
		}
	}

	query := elastic.NewMultiMatchQuery(params.Q, "piece", "composer", "joined_performers").
		Operator("and")

	total, items, err := params.findPerforms(query.Type("cross_fields"), cu)
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	if total == 0 {
		// пытаемся исправить опечатки
		query = elastic.NewMultiMatchQuery(params.Q, "piece", "composer", "joined_performers").
			Operator("and")
		total, items, err = params.findPerforms(
			query.
				Field("composer").
				Field("joined_performers").
				Field("piece").
				Fuzziness("AUTO"),
			cu,
		)
		if err != nil {
			ctx.AbortWithError(500, err)
			return
		}
	}

	ctx.JSON(200, gin.H{
		"total": total,
		"data":  items,
	})
}

// @Summary Performs Favorite
// @Description get favorite performs
// @Accept json
// @Produce json
// @Success 200 {array} models.PerformResult
// @Failure 403 {string} string "Access Denied"
// @Router /api/performs/favorite [get]
func PerformsFavorite(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	scope := models.FavoritePerformScope(cu)
	params := GetSearchQueryParams(ctx)
	performs := []*models.Perform{}
	if params.ComposerID > 0 {
		scope = scope.Where("piece_id IN (SELECT id from pieces WHERE composer_id = ?)", params.ComposerID)
	}
	if params.PieceID > 0 {
		scope = scope.Where("piece_id  = ?", params.PieceID)
	}
	if params.PerformerID > 0 {
		scope = scope.Where("group_id IN (SELECT group_id FROM group_performers WHERE performer_id = ?)", params.PerformerID)
	}
	scope = scope.Order("likes desc")
	err := scope.Limit(params.Size).Offset(params.From).Find(&performs).Error
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	var total int
	err = scope.Count(&total).Error
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	items, err := PG2json(performs, cu)
	if err != nil {
		ctx.AbortWithError(500, errors.Wrap(err, "pg2json"))
		return
	}
	ctx.JSON(200, gin.H{
		"total": total,
		"data":  items,
	})

}

// @Summary Performs Show
// @Description get performs
// @Accept json
// @Produce json
// @Param perform_id path int true "Id of the perfom"
// @Success 200 {object} models.PerformResult
// @Failure 403 {string} string "Access Denied"
// @Router /api/perform/{perform_id} [get]
func PerformsShow(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	performID, err := strconv.Atoi(ctx.Param("perform_id"))
	if err != nil {
		ctx.AbortWithError(404, err)
		return
	}
	performs := []*models.Perform{}
	err = models.PerformSearchScope().Where("id = ?", performID).Find(&performs).Error
	if err != nil {
		ctx.AbortWithError(404, err)
		return
	}
	if len(performs) != 1 {
		ctx.AbortWithError(404, errors.New("unable to find one perform"))
		return
	}
	items, err := PG2json(performs, cu)
	if err != nil {
		ctx.AbortWithError(404, errors.Wrap(err, "pg2json"))
		return
	}
	ctx.JSON(200, items[0])
}

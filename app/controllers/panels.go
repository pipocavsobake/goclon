package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"
)

type PanelResult struct {
	Title string                  `json:"title"`
	Data  []models.PerformApiData `json:"data"`
}

type PanelsResult [3]PanelResult

func Panels(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		return
	}
	var result PanelsResult
	result[0].Title = "Прослушанное"
	result[1].Title = "Добавленное"
	result[2].Title = "Рекомендованное"
	if err := result.Fill(cu); err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error(), 0})
		return
	}
	ctx.JSON(http.StatusOK, result)
}

func (result *PanelsResult) Fill(cu *models.SiteUser) (err error) {
	funs := []func(*models.SiteUser) ([]*models.Perform, error){
		models.ListenedPerforms,
		models.AddedPerforms,
		models.RecommendedPerforms,
	}
	for i, fun := range funs {
		(*result)[i].Data = []models.PerformApiData{}
		var performs []*models.Perform
		if performs, err = fun(cu); err != nil {
			return
		} else {
			if (*result)[i].Data, err = PG2json(performs, cu); err != nil {
				return
			}
		}
	}
	return nil
}

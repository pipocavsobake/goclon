package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"

	"github.com/davecgh/go-spew/spew"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

func FindOrCreateFight(pieceID int64, cu *models.SiteUser) (fight models.Fight, err error) {
	if err = models.FightSearchScope().Where("piece_id = ? AND site_user_id = ?", pieceID, cu.ID).First(&fight).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			pid := int64(pieceID)
			fight.PieceID = &pid
			fight.SiteUserID = &cu.ID
			if err = models.DB.Create(&fight).Error; err != nil {
				return
			}
		} else {
			return
		}
	}
	return
}

func loadPairPerforms(id1, id2 int64, cu *models.SiteUser) (p1, p2 models.PerformApiData, err error) {
	performs := []*models.Perform{}
	if err = models.PerformSearchScope().Where("id IN (?)", []int64{id1, id2}).Find(&performs).Error; err != nil {
		return
	}
	if len(performs) != 2 {
		return models.PerformApiData{}, models.PerformApiData{}, errors.New(fmt.Sprintf("Мистика %v", len(performs)))
	}
	jsons, err := PG2json(performs, cu)
	if err != nil {
		return models.PerformApiData{}, models.PerformApiData{}, errors.Wrap(err, "pg2json")
	}
	for _, perf := range jsons {
		if perf.ID == id1 {
			p1 = perf
			continue
		}
		p2 = perf
	}
	return
}

func getPerformsForFight(fight models.Fight, cu *models.SiteUser) (best, next models.PerformApiData, fightID int64, err error) {
	var bestID, nextID int64
	if bestID, nextID, err = fight.GetPairIDs(); err != nil {
		return
	}
	best, next, err = loadPairPerforms(bestID, nextID, cu)
	return
}

type FightResult struct {
	ID    int64                   `json:"id"`
	Fight models.Fight            `json:"fight"`
	Best  models.PerformApiData   `json:"best"`
	Next  models.PerformApiData   `json:"next"`
	Data  []models.PerformApiData `json:"data"`
	Done  bool                    `json:"done"`
}

type ApiError struct {
	Error   string `json:"error"`
	FightID int64  `json:"fight_id"`
}

// @Summary Show Fight
// @Description get Fight
// @Accept json
// @Produce json
// @Param id path int true "Id of fight"
// @Success 200 {object} controllers.FightResult
// @Failure 403 {string} string "Access Denied"
// @Failure 400 {object} controllers.ApiError "cannot parse id"
// @Failure 404 {object} controllers.ApiError "there are no fight for current user provided this id"
// @Router /api/fights/{id} [get]
func ShowFight(ctx *gin.Context) {
	cu := concerns.Auth(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error(), 0})
		return
	}
	var fight models.Fight
	if err := models.FightSearchScope().Where("id = ? and site_user_id = ?", id, cu.ID).First(&fight).Error; err != nil {
		ctx.JSON(http.StatusNotFound, ApiError{err.Error(), 0})
	}
	showFight(ctx, fight, cu)
}

func showFight(ctx *gin.Context, fight models.Fight, cu *models.SiteUser) {
	best, next, _, err := getPerformsForFight(fight, cu)
	done := false
	if err != nil {
		if err == models.FightDoneErr {
			done = true
		} else {
			ctx.JSON(400, ApiError{err.Error(), fight.ID})
			return
		}
	}
	performs, _, err := fight.BestPerforms()
	if err != nil {
		ctx.JSON(400, ApiError{err.Error(), fight.ID})
		return
	}
	data, err := PG2json(performs, cu)
	if err != nil {
		ctx.JSON(400, ApiError{err.Error(), fight.ID})
	}
	ctx.JSON(200, FightResult{fight.ID, fight, best, next, data, done})
}

type CreateFightParams struct {
	PieceID int64 `json:"piece_id" example:"1"`
}

// @Summary CreateFight
// @Description create Fight
// @Accept json
// @Produce json
// @Param fight body controllers.CreateFightParams true "fight data"
// @Success 200 {object} controllers.FightResult
// @Failure 403 {string} string "Access Denied"
// @Failure 400 {object} controllers.ApiError "cannot parse id"
// @Failure 404 {object} controllers.ApiError "there are no fight for current user provided this id"
// @Failure 422 {object} controllers.ApiError "invalid request body"
// @Router /api/fights [post]
func CreateFight(ctx *gin.Context) {
	cu := concerns.Auth(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	var params CreateFightParams
	var err error
	if err = ctx.ShouldBindJSON(&params); err != nil {
		ctx.JSON(422, ApiError{err.Error(), 0})
		return
	}
	var fight models.Fight
	if fight, err = FindOrCreateFight(params.PieceID, cu); err != nil {
		ctx.JSON(422, ApiError{err.Error(), 0})
		return
	}
	showFight(ctx, fight, cu)
}

type FightIndexResult struct {
	Total int64          `json:"total" example:"12"`
	Data  []models.Fight `json:"data"`
}

// @Summary IndexFights
// @Description index Fight
// @Accept json
// @Produce json
// @Param panel query string false "latest, top, finished"
// @Success 200 {object} controllers.FightIndexResult
// @Failure 403 {string} string "Access Denied"
// @Failure 404 {object} controllers.ApiError "there are no fights for current user"
// @Router /api/fights [get]
func IndexFights(ctx *gin.Context) {
	cu := concerns.Auth(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	params := GetSearchQueryParams(ctx)
	fights := make([]models.Fight, 0)
	scope := models.FightSearchScope().Where("site_user_id = ?", cu.ID)
	var total int64
	if err := scope.Count(&total).Error; err != nil {
		ctx.JSON(http.StatusNotFound, ApiError{err.Error(), 0})
		return
	}
	if err := scope.Limit(params.Size).Offset(params.From).Find(&fights).Error; err != nil {
		ctx.JSON(http.StatusNotFound, ApiError{err.Error(), 0})
		return
	}
	ctx.JSON(http.StatusOK, FightIndexResult{total, fights})
}

type FightParams struct {
	NextID     int64  `json:"next_id" binding:"required" example:"1"`
	BestID     int64  `json:"best_id" binding:"required" example:"2"`
	Decision   string `json:"decision" binding:"required" example:"WIN" enums:"WIN,UNCOMPARABLE,DO_NOT_KNOW"`
	PreferedID int64  `json:"prefered_id" example:"1"`
	ClientTime int64  `json:"client_time" binding:"required" example:"10515252"`
}

// @Summary EditFight
// @Description edit Fight
// @Accept json
// @Produce json
// @Param fight body controllers.FightParams true "fight data"
// @Param id path int true "Id of fight"
// @Success 200 {object} controllers.FightResult
// @Failure 403 {string} string "Access Denied"
// @Failure 400 {object} controllers.ApiError "cannot parse id"
// @Failure 404 {object} controllers.ApiError "there are no fight for current user provided this id"
// @Failure 422 {object} controllers.ApiError "invalid request body"
// @Router /api/fights/{id} [put]
func EditFight(ctx *gin.Context) {
	cu := concerns.Auth(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Anauthorized"))
		return
	}
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error(), 0})
		return
	}

	var fightParams FightParams
	if err := ctx.ShouldBindJSON(&fightParams); err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, ApiError{err.Error(), 0})
		return
	}

	var fight models.Fight
	if err := models.FightSearchScope().Where("id = ? and site_user_id = ?", id, cu.ID).First(&fight).Error; err != nil {
		ctx.JSON(http.StatusNotFound, ApiError{err.Error(), 0})
		return
	}
	//  WIN, LOSE, UNCOMPARABLE, DO_NOT_KNOW
	switch fightParams.Decision {
	case "WIN":
		otherID := fightParams.NextID
		if otherID == fightParams.PreferedID {
			otherID = fightParams.BestID
		} else {
			if fightParams.BestID != fightParams.PreferedID {
				ctx.JSON(http.StatusBadRequest, ApiError{"prefered_id is not in (best_id, other_id)", 0})
				return
			}
		}
		if err := fight.SetWinner(fightParams.PreferedID, otherID, fightParams.ClientTime); err != nil {
			ctx.JSON(http.StatusInternalServerError, ApiError{err.Error(), 0})
			return
		}
		pr := models.Preference{WinnerID: &fightParams.PreferedID, LoserID: &otherID, SiteUserId: &cu.ID}
		if err := models.DB.Save(&pr); err != nil {
			spew.Dump(err)
		}
		ctx.Redirect(http.StatusFound, fmt.Sprintf("/api/fight/%v", fight.ID))
		break
	case "UNCOMPARABLE":
		if err := fight.SetEqual(fightParams.NextID, fightParams.BestID, fightParams.ClientTime); err != nil {
			ctx.JSON(http.StatusInternalServerError, ApiError{err.Error(), 0})
			return
		}
		break
	case "DO_NOT_KNOW":
		break
	default:
		ctx.JSON(http.StatusUnprocessableEntity, ApiError{"unknown decision type", 0})
		return
	}
	if err := models.FightSearchScope().Where("id = ? and site_user_id = ?", id, cu.ID).First(&fight).Error; err != nil {
		ctx.JSON(http.StatusInternalServerError, ApiError{err.Error(), 0})
		return
	}
	showFight(ctx, fight, cu)
}

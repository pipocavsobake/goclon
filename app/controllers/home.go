package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
)

func HomeIndex(ctx *gin.Context) {
	user := concerns.Auth(ctx)
	if user == nil {
		ctx.Redirect(http.StatusFound, "/auth/login")
		return
	}
	//ctx.Redirect(http.StatusFound, "/admin")
	ctx.Status(http.StatusOK)
	Render.Layout("base")
	Render.Execute("home/index", gin.H{}, ctx.Request, ctx.Writer)
}

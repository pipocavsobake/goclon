package controllers

import (
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/pipocavsobake/goclon/app/controllers/concerns"
	"gitlab.com/pipocavsobake/goclon/app/models"
)

type EventForm struct {
	PerformID int64  `form:"perform_id" json:"perform_id" binding:"required" example:"1"`
	Kind      string `form:"kind" json:"kind" binding:"required" enums:"SELECT,PAUSE,STOP,UNEXPECTED_STOP,FINISH,PLAY" example:"PLAY"`
	Value     int64  `form:"value" json:"value" example:"15436"`
	Timestamp int64  `form:"timestamp" json:"timestamp" binding:"required" example:"1548781754000000"`
}

// @Summary Create Event
// @Description create user-event about listening a record
// @Accept json
// @Produce json
// @Param body body EventForm true "perform_id(int), kind(string), value(int), timestamp(int)"
// @Success 204 {object} models.EmptyResult
// @Router /api/events [post]
func CreateEvent(ctx *gin.Context) {
	cu := concerns.AuthJSON(ctx)
	if cu == nil {
		ctx.AbortWithError(401, errors.New("Unauthorized"))
		return
	}
	var form EventForm
	if err := ctx.ShouldBindJSON(&form); err != nil {
		ctx.AbortWithError(422, err)
		return
	}
	perf := models.Perform{}
	models.DB.Where("id = ?", form.PerformID).First(&perf)
	if perf.ID == 0 {
		ctx.AbortWithError(404, errors.New("Cannot find Perform"))
		return
	}
	event := models.Event{}
	t := time.Unix(form.Timestamp/1000, (form.Timestamp%1000)*1000*1000)
	if err := models.DB.Where(&models.Event{PerformID: &perf.ID, SiteUserID: &cu.ID}).First(&event).Error; err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}
	if event.ID == 0 {
		ev := models.Event{
			PerformID:  &perf.ID,
			SiteUserID: &cu.ID,
			ClientTime: &t,
			Kind:       "FIRST_TOUCH",
		}
		if err := models.DB.Save(&ev).Error; err != nil {
			ctx.AbortWithError(http.StatusBadRequest, err)
			return
		}
	}
	if err := models.DB.FirstOrCreate(
		&event,
		models.Event{
			PerformID:  &perf.ID,
			SiteUserID: &cu.ID,
			ClientTime: &t,
			Value:      form.Value,
			Kind:       form.Kind,
		},
	).Error; err != nil {
		ctx.AbortWithError(422, err)
		return
	}
	ctx.AbortWithStatus(204)
}

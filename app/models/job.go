package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Job struct {
	gorm.Model
	Kind      string
	InputData string
	State     string
	Progress  int64
	Total     int64
	Error     string
}

func (j *Job) Init(total int) {
	j.State = "new"
	j.Total = int64(total)
	DB.Save(j)
}

func (j *Job) Add(i int) {
	j.State = "processing"
	j.Progress += int64(i)
	DB.Save(j)
}

func (j *Job) Set(i int) {
	j.State = "processing"
	j.Progress = int64(i)
	DB.Save(j)
}

func (j *Job) Finish() {
	j.State = "done"
	j.Progress = j.Total
	DB.Save(j)
}

func (j *Job) Fail(m string) {
	j.State = "failed"
	j.Error = m
	DB.Save(j)
}

func (a *Job) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Job{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

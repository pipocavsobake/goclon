package models

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Preference struct {
	ID         int64      `gorm:"primary_key" json:"id"`
	WinnerID   *int64     `gorm:"type:bigint REFERENCES performs(id)" json:"winner_id"`
	Winner     *Perform   `json:"winner"`
	Loser      *Perform   `json:"loser"`
	LoserID    *int64     `gorm:"type:bigint REFERENCES performs(id)" json:"loser_id"`
	SiteUserId *int64     `gorm:"type:bigint REFERENCES site_users(id)" json:"site_user_id"`
	SiteUser   *SiteUser  `gorm:"site_user" json:"site_user"`
	CreatedAt  *time.Time `json:"created_at"`
	UpdatedAt  *time.Time `json:"updated_at"`
}

func (a *Preference) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Preference{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

func (a *Preference) Authorize(scope *gorm.DB, cu *SiteUser) {
	if cu == nil {
		*scope = *scope.Where("1 = 0")
		return
	}
	*scope = *scope.Where("site_user_id = ?", cu.ID)
}

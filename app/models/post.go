package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Post struct {
	ID       int64   `gorm:"primary_key" json:"id"`
	Url      string  `gorm:"unique_index" json:"url"`
	Content  string  `gorm:"type:text" json:"content"`
	Status   string  `json:"status"`
	SourceID *int64  `gorm:"type:bigint REFERENCES sources(id)" json:"source_id"`
	Source   *Source `json:"source"`
}

func (a *Post) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Post{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

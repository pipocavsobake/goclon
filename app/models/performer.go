package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Performer struct {
	ID              int64             `gorm:"primary_key" json:"id"`
	ClassicOnlineId string            `gorm:"unique_index" json:"classic_online_id"`
	Instrument      string            `json:"instrument"`
	Url             string            `json:"url"`
	GroupPerformers []*GroupPerformer `json:"group_performers"`
	Name            string            `json:"name"`
}

func (a *Performer) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Perform{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

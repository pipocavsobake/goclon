package models

type Group struct {
	ID              int64             `gorm:"primary_key" json:"id"`
	GroupPerformers []*GroupPerformer `json:"group_performers"`
	Performers      []Performer       `gorm:"many2many:group_performers;" json:"performers"`
}

package models

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
)

type Article struct {
	ID         int64      `gorm:"primary_key" json:"id"`
	Name       string     `gorm:"not null" json:"name"`
	Slug       *string    `gorm:"not null unique_index" json:"slug"`
	Content    string     `gorm:"type:text" json:"content"`
	SiteUserId *int64     `gorm:"type:bigint REFERENCES site_users(id)" json:"site_user_id"`
	SiteUser   *SiteUser  `gorm:"site_user"`
	CreatedAt  *time.Time `json:"created_at"`
	UpdatedAt  *time.Time `json:"updated_at"`
}

func (a *Article) BeforeSave() {
	if a.Slug == nil {
		s := slug.Make(a.Name)
		a.Slug = &s
	}
}

func (a *Article) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Article{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Mark struct {
	ID         int64  `gorm:"primary_key"`
	CommentId  *int64 `gorm:"type:bigint REFERENCES comments(id)"`
	Comment    *Comment
	Kind       string
	Value      string
	Approval   *bool
	SiteUserId *int64 `gorm:"type:bigint REFERENCES site_users(id)"`
	SiteUser   *SiteUser
}

func (a *Mark) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Mark{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

func (a *Mark) Authorize(scope *gorm.DB, cu *SiteUser) {
	if cu == nil {
		*scope = *scope.Where("1 = 0")
		return
	}
	*scope = *scope.Where("site_user_id = ?", cu.ID)
}

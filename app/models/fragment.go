package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Fragment struct {
	ID        int64 `gorm:"primary_key" json:"id"`
	PerformID int64 `gorm:"type:bigint REFERENCES performs(id)" json:"perform_id"`
	Start     int64 `json:"start"`
	End       int64 `json:"end"`
	Sort      int64 `json:"sort"`
}

func (a *Fragment) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Fragment{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

func (a *Fragment) Authorize(scope *gorm.DB, cu *SiteUser) {
	if cu == nil {
		*scope = *scope.Where("1 = 0")
		return
	}
	*scope = *scope.Where("site_user_id = ?", cu.ID)
}

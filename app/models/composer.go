package models

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Composer struct {
	ID              int64    `gorm:"primaty_key" json:"id"`
	ClassicOnlineId string   `gorm:"unique_index" json:"classic_online_id"`
	Name            string   `json:"name"`
	Pieces          []*Piece `json:"pieces" api:"-"`
	BirthYear       *int64   `json:"-" api:"-"`
	DeathYear       *int64   `json:"-" api:"-"`
}

func FindComposerByUrl(url string) *Composer {
	ret := &Composer{}
	DB.Where(&Composer{ClassicOnlineId: strings.TrimPrefix(url, "http://classic-online.ru")}).Find(ret)
	if ret.ID == 0 {
		return nil
	}
	return ret
}

func (a *Composer) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Composer{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

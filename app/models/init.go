package models

import (
	"context"

	"github.com/jinzhu/gorm"
	elastic "github.com/olivere/elastic/v7"
	"github.com/qor/media"
	"github.com/qor/validations"
	"gitlab.com/pipocavsobake/goclon/config/database"
	"gitlab.com/pipocavsobake/goclon/config/logger"
)

var DB *gorm.DB
var Config map[string]string
var ES *elastic.Client
var PERFORM_INDEX string

func pErr(err error) {
	if err != nil {
		panic(err)
	}
}

func init() {
	var err error
	DB = database.DB
	validations.RegisterCallbacks(DB)
	DB.LogMode(true)
	media.RegisterCallbacks(DB)
	//err = DB.SetupJoinTable(&Group{}, "Performers", &GroupPerformer{})
	//pErr(err)
	//err = DB.SetupJoinTable(&SiteUser{}, "Performs", &SiteUserPerform{})
	//pErr(err)
	err = DB.AutoMigrate(&User{}, &Composer{}, &Group{}, &Performer{},
		&GroupPerformer{}, &Piece{}, &Perform{}, &Comment{}, &SiteUser{},
		&Mark{}, &Job{}, &SiteUserPerform{}, &Text{}, &Directory{},
		&PerformDirectory{}, &Event{}, &Source{}, &Post{}, &SearchSub{},
		&Fragment{}, &Fight{}, &FightPerform{}, &Article{}, &Preference{}).Error
	pErr(err)
	DB.Model(&PerformDirectory{}).AddUniqueIndex(
		"perform_id_directory_id_idx",
		"perform_id",
		"directory_id",
	)
	DB.Model(&Directory{}).AddUniqueIndex(
		"directory_name_user",
		"name",
		"site_user_id",
	)
	DB.Model(&Mark{}).AddUniqueIndex(
		"comment_id_site_user_id_idx",
		"comment_id",
		"site_user_id",
	)
	DB.Model(&SiteUserPerform{}).AddUniqueIndex(
		"site_user_id_perform_id_idx",
		"site_user_id",
		"perform_id",
	)
	DB.Model(&FightPerform{}).AddUniqueIndex(
		"perform_id_fight_id_idx",
		"perform_id",
		"fight_id",
	)
	ctx := context.Background()
	elastic.SetInfoLog(logger.Logger)
	ES, err = elastic.NewClient(elastic.SetURL(*database.Config.ElasticURL))
	pErr(err)
	var prefix string
	if database.Config.MappingPrefix != nil {
		prefix = *database.Config.MappingPrefix
	}
	PERFORM_INDEX = prefix + "performs"
	exists, err := ES.IndexExists(PERFORM_INDEX).Do(ctx)
	pErr(err)
	if !exists {
		createIndex, err := ES.CreateIndex(PERFORM_INDEX).BodyString(PerformMapping).Do(ctx)
		pErr(err)
		if !createIndex.Acknowledged {
			panic("not acknowledged")
		}
	}
}

type PerformResult struct {
	Total int64
	Data  []PerformSearchData
}

type DirectoryResult struct {
	Total int64
	Items []SafeDirectory
}
type SafeDirectory struct {
	ID   int64  `json:"ID"`
	Name string `json:"name"`
}
type OneDirResult struct {
	Item SafeDirectory
}

type EmptyResult struct{}

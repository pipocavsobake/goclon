package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Source struct {
	ID   int64  `gorm:"primary_key"`
	Name string `gorm:"unique_index"`
	Host string
}

func (a *Source) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Source{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

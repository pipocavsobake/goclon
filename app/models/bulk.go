package models

import (
	"github.com/jinzhu/gorm"
)

func BulkPerform(scope *gorm.DB, bulkFunction func([]*Perform, int, int) bool) {
	performs := []*Perform{}
	count := 0
	limit := 1000
	scope.Count(&count)
	if count == 0 {
		panic("count = 0")
	}
	for i := 0; i < count; i += limit {
		scope.Limit(limit).Offset(i).Find(&performs)
		if !bulkFunction(performs, i, count) {
			return
		}
	}
}

func EachPerform(scope *gorm.DB, f func(*Perform, int, int) bool) {
	BulkPerform(scope, func(performs []*Perform, offset, total int) bool {
		for i, perform := range performs {
			if !f(perform, offset+i, total) {
				return false
			}
		}
		return true
	})
}

func BulkComposer(scope *gorm.DB, bulkFunction func([]*Composer, int, int)) {
	composers := []*Composer{}
	count := 0
	limit := 100
	scope.Count(&count)
	if count == 0 {
		panic("count = 0")
	}
	for i := 0; i < count; i += limit {
		scope.Limit(limit).Offset(i).Find(&composers)
		bulkFunction(composers, i, count)
	}
}

func EachComposer(scope *gorm.DB, f func(*Composer, int, int)) {
	BulkComposer(scope, func(composers []*Composer, offset, total int) {
		for i, composer := range composers {
			f(composer, offset+i, total)
		}
	})
}

package models

import "time"

type FightPerform struct {
	ID         int64      `gorm:"primary_key" json:"id"`
	FightID    *int64     `gorm:"type:bigint REFERENCES fights(id)" valid:"required" json:"fight_id"`
	PerformID  *int64     `gorm:"type:bigint REFERENCES performs(id)" valid:"required" json:"perform_id"`
	Perform    *Perform   `json:"-"`
	Position   *int64     `json:"position"`
	ClientTime *time.Time `valid:"required" json:"client_time"`
}

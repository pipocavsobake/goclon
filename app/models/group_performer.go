package models

type GroupPerformer struct {
	ID          int64  `gorm:"primary_key"`
	PerformerID *int64 `gorm:"type:bigint REFERENCES performers(id)"`
	Performer   *Performer
	GroupID     *int64 `gorm:"type:bigint REFERENCES groups(id)"`
	Group       *Group
}

package models

import (
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	"fmt"
	"time"
)

type Fight struct {
	ID            int64           `gorm:"primary_key" json:"id"`
	SiteUserID    *int64          `gorm:"type:bigint REFERENCES site_users(id)" json:"site_user_id"`
	PieceID       *int64          `gorm:"type:bigint REFERENCES performs(id)" json:"piece_id"`
	Piece         *Piece          `json:"piece"`
	FightPerforms []*FightPerform `json:"fight_performs"`
}

func (f *Fight) Undone() ([]Perform, error) {
	performs := []Perform{}
	ids := make([]int64, len(f.FightPerforms))
	for i, fp := range f.FightPerforms {
		ids[i] = *fp.PerformID
	}
	err := DB.Model(&Perform{}).Where("piece_id = ? AND id IN (?)", *f.PieceID, ids).Find(&performs).Error
	return performs, err
}

func (f *Fight) UndoneIDs() ([]int64, error) {
	ids := make([]int64, len(f.FightPerforms))
	for i, fp := range f.FightPerforms {
		ids[i] = *fp.PerformID
	}
	ret := []int64{}
	ids = append(ids, 0) // gorm как-то криво вставляет пустой слайс
	err := DB.Model(&Perform{}).Where("piece_id = ? AND id NOT IN (?)", *f.PieceID, ids).Pluck("id", &ret).Error
	return ret, errors.Wrap(err, "undone ids")
}

var FightDoneErr = errors.New("Fight done")

func (fight *Fight) GetPairIDs() (a int64, b int64, err error) {
	var undone []int64
	if undone, err = fight.UndoneIDs(); err != nil {
		return
	}
	fps := fight.FightPerforms
	var bestID, nextID, prevID *int64
	var clientTime time.Time
	for _, fightPerform := range fps {
		if fightPerform.Position != nil && *fightPerform.Position == 1 {
			bestID = fightPerform.PerformID
		}
		if fightPerform.Position == nil && nextID == nil {
			nextID = fightPerform.PerformID
			clientTime = *fightPerform.ClientTime
		}
		if fightPerform.Position == nil && nextID != nil {
			if (*fightPerform.ClientTime).Before(clientTime) {
				prevID = nextID
				nextID = fightPerform.PerformID
				clientTime = *fightPerform.ClientTime
			}
		}
	}
	if bestID != nil && len(undone) > 0 { // есть фаворит, есть неголосованный
		return *bestID, undone[0], nil
	}
	if bestID == nil && len(undone) > 1 { // нет фаворита, есть 2 неголосованных
		return undone[0], undone[1], nil
	}
	if bestID == nil && len(undone) == 1 && nextID != nil { // нет фаворита, один неголосованный, один голосованный (по ответу не знаю или несравнимо)
		return *nextID, undone[0], nil
	}
	if len(undone) == 0 && (nextID == nil || (bestID == nil && prevID == nil)) { // кончились кандидаты
		return 0, 0, FightDoneErr
	}
	if bestID == nil && len(undone) == 0 && nextID != nil && prevID != nil { // нет фаворита, два голосованных, нет неголосованных
		return *prevID, *nextID, nil
	}
	return *bestID, *nextID, nil
}

func FightSearchScope() *gorm.DB {
	return DB.Model(&Fight{}).Preload("FightPerforms").Preload("Piece.Composer")
}

func (fight *Fight) SetWinner(id, otherId, ts int64) (err error) {
	//if len(fight.FightPerforms) == 0 {
	//return errors.New("len(fight.FightPerforms) == 0")
	//}
	tx := DB.Begin()
	var bestDone, otherDone bool
	spew.Dump(fight)
	spew.Dump(id)
	spew.Dump(otherId)
	t := time.Unix(ts/1000, (ts%1000)*1000*1000)
	for _, fp := range fight.FightPerforms {
		if fp.PerformID != nil && *fp.PerformID == id {
			*fp.Position = 1
			fp.ClientTime = &t
			if err = tx.Save(&fp).Error; err != nil {
				tx.Rollback()
				return
			}
			bestDone = true
		} else {
			if err = tx.Model(&fp).Update("position = position + 1").Error; err != nil {
				tx.Rollback()
				return
			}
			if fp.PerformID != nil && *fp.PerformID == otherId {
				otherDone = true
			}
		}
	}
	if !bestDone {
		fid := fight.ID
		pos := int64(1)
		fp := FightPerform{FightID: &fid, Position: &pos, PerformID: &id, ClientTime: &t}
		if err = tx.Save(&fp).Error; err != nil {
			return
		}
	}
	if !otherDone {
		fid := fight.ID
		pos := int64(2)
		fp := FightPerform{FightID: &fid, Position: &pos, PerformID: &otherId, ClientTime: &t}
		if err = tx.Save(&fp).Error; err != nil {
			return
		}
	}
	return tx.Commit().Error
}

func (fight *Fight) SetEqual(nextID, bestID, ts int64) (err error) {
	if len(fight.FightPerforms) == 0 {
		return errors.New("len(fight.FightPerforms) == 0")
	}
	var next, best FightPerform
	for _, fp := range fight.FightPerforms {
		if fp.ID == nextID {
			next = *fp
		} else if fp.ID == bestID {
			best = *fp
		}
	}
	next.Position = best.Position
	return DB.Save(&next).Error
}

func (fight *Fight) BestPerforms() (performs []*Perform, positions []int64, err error) {
	fps := fight.FightPerforms
	if len(fps) == 0 || fps[0].Perform == nil {
		if err = DB.Model("FightPerform").Preload("Perform").Where("fight_id = ? AND position = 1", fight.ID).Find(&fps).Error; err != nil {
			return
		}
	}
	performs = make([]*Perform, 0)
	positions = make([]int64, 0)
	for _, fp := range fps {
		if fp.Perform == nil {
			err = errors.New(fmt.Sprintf("fp.Perform(id=%v)==nil", fp.ID))
			return
		}
		if *fp.Position != 1 {
			continue
		}
		performs = append(performs, fp.Perform)
		positions = append(positions, *fp.Position)
	}
	return
}

func (a *Fight) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Fight{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

func (a *Fight) Authorize(scope *gorm.DB, cu *SiteUser) {
	if cu == nil {
		*scope = *scope.Where("1 = 0")
		return
	}
	*scope = *scope.Where("site_user_id = ?", cu.ID)
}

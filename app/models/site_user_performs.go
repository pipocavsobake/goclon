package models

type SiteUserPerform struct {
	ID         int64  `gorm:"primary_key"`
	SiteUserID *int64 `gorm:"type:bigint REFERENCES site_users(id)"`
	SiteUser   *SiteUser
	PerformID  *int64 `gorm:"type:bigint REFERENCES performs(id)"`
	Perform    *Perform
	Like       bool
	Count      int64
}

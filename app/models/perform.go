package models

import (
	"context"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Perform struct {
	ID                 int64              `gorm:"primary_key" json:"id"`
	ClassicOnlineId    string             `gorm:"unique_index" json:"classic_online_id"`
	Date               *time.Time         `json:"date"`
	UserID             *int64             `gorm:"type:bigint REFERENCES users(id)" json:"user_id"`
	User               *User              `json:"user"`
	Likes              int64              `json:"likes"`
	PieceID            *int64             `gorm:"type:bigint REFERENCES pieces(id)" json:"piece_id"`
	Piece              *Piece             `json:"piece"`
	GroupID            *int64             `gorm:"type:bigint REFERENCES groups(id)" json:"group_id"`
	Group              *Group             `json:"group"`
	ContentLength      int64              `json:"content_length"`
	DlStatus           string             `json:"dl_status"`
	HasAudio           bool               `json:"has_audio"`
	Comments           []Comment          `json:"comments"`
	PerformDirectories []PerformDirectory `json:"perform_directories"`
}

type PerformSearchData struct {
	ClassicOnlineId  string     `json:"classic_online_id"`
	Date             *time.Time `json:"upload_date"`
	DateK            *time.Time `json:"date.keyword"`
	User             string     `json:"user"`
	Likes            int64      `json:"likes"`
	Piece            string     `json:"piece"`
	PieceID          int64      `json:"piece_id"`
	Composer         string     `json:"composer"`
	Performers       []string   `json:"performers"`
	JoinedPerformers string     `json:"joined_performers"`
	HasAudio         bool       `json:"has_audio"`
	HasAudioK        bool       `json:"has_audiok.keyword"`
	CommentsCount    int64      `json:"comments_count"`
	CommentsMax      int64      `json:"comments_max"`
	CommentsSum      int64      `json:"comments_sum"`
	Summary          []string   `json:"summary"`
}
type PerformApiData struct {
	ID               int64              `json:"id"`
	ClassicOnlineId  string             `json:"classic_online_id"`
	Date             *time.Time         `json:"upload_date"`
	DateK            *time.Time         `json:"date.keyword"`
	User             string             `json:"user"`
	Likes            int64              `json:"likes"`
	Piece            string             `json:"piece"`
	PieceID          int64              `json:"piece_id"`
	Composer         string             `json:"composer"`
	Performers       []string           `json:"performers"`
	JoinedPerformers string             `json:"joined_performers"`
	HasAudio         bool               `json:"has_audio"`
	HasAudioK        bool               `json:"has_audiok.keyword"`
	CommentsCount    int64              `json:"comments_count"`
	CommentsMax      int64              `json:"comments_max"`
	CommentsSum      int64              `json:"comments_sum"`
	Directories      []DirectoryApiData `json:"directories"`
	IsFavorite       bool               `json:"is_favorite"`
	LastMS           int64              `json:"last_ms"`
	BestPerforms     []*PerformApiData  `json:"best_performs"`
	Position         *int64             `json:"position"`
	ContentLength    *int64             `json:"content_length"`
}

func (p *Perform) UserName() string {
	if user := p.User; user != nil {
		return strings.Trim(user.Name, "\n")
	}
	return ""
}

func (p *Perform) PieceName() string {
	if piece := p.Piece; piece != nil {
		return strings.Trim(piece.Name, "\n")
	}
	return ""
}

func (p *Perform) ComposerName() string {
	if p.Piece == nil {
		return ""
	}
	if composer := p.Piece.Composer; composer != nil {
		return strings.Trim(composer.Name, "\n")
	}
	return ""
}

func (p *Perform) PerformerNames() []string {
	ret := []string{}
	if p.Group == nil {
		return ret
	}
	for _, gp := range p.Group.GroupPerformers {
		if performer := gp.Performer; performer != nil {
			ret = append(ret, strings.Trim(performer.Name, "\n"))
		}
	}
	return ret
}

func (p *Perform) CommentsCount() int64 {
	return int64(len(p.Comments))
}

func (p *Perform) CommentsMax() (ret int64) {
	for _, comment := range p.Comments {
		if comment.Likes > ret {
			ret = comment.Likes
		}
	}
	return
}

func (p *Perform) CommentsSum() (ret int64) {
	for _, comment := range p.Comments {
		ret += comment.Likes
	}
	return
}

func (p *Perform) SearchData() PerformSearchData {
	pieceID := int64(0)
	if p.PieceID != nil {
		pieceID = *p.PieceID
	}
	return PerformSearchData{
		ClassicOnlineId:  p.ClassicOnlineId,
		Date:             p.Date,
		DateK:            p.Date,
		User:             p.UserName(),
		Likes:            p.Likes,
		Piece:            p.PieceName(),
		PieceID:          pieceID,
		Composer:         p.ComposerName(),
		Performers:       p.PerformerNames(),
		JoinedPerformers: strings.Join(p.PerformerNames(), " "),
		HasAudio:         p.HasAudio,
		HasAudioK:        p.HasAudio,
		CommentsCount:    p.CommentsCount(),
		CommentsMax:      p.CommentsMax(),
		CommentsSum:      p.CommentsSum(),
		Summary:          append(p.PerformerNames(), p.ComposerName(), p.PieceName()),
	}
}

func (p *Perform) SearchDataWithID() PerformApiData {
	cl := &p.ContentLength
	if *cl == 0 {
		cl = nil
	}
	return PerformApiData{
		ID:               p.ID,
		ClassicOnlineId:  p.ClassicOnlineId,
		Date:             p.Date,
		DateK:            p.Date,
		User:             p.UserName(),
		Likes:            p.Likes,
		Piece:            p.PieceName(),
		PieceID:          *p.PieceID,
		Composer:         p.ComposerName(),
		Performers:       p.PerformerNames(),
		JoinedPerformers: strings.Join(p.PerformerNames(), " "),
		HasAudio:         p.HasAudio,
		HasAudioK:        p.HasAudio,
		CommentsCount:    p.CommentsCount(),
		CommentsMax:      p.CommentsMax(),
		CommentsSum:      p.CommentsSum(),
		ContentLength:    cl,
	}
}

func PerformSearchScope() *gorm.DB {
	return DB.Model(&Perform{}).
		Preload("User").
		Preload("Piece.Composer").
		Preload("Group.GroupPerformers.Performer").
		Preload("Comments")
}

func (p *Perform) Reindex() (err error) {
	perf := Perform{}
	scope := PerformSearchScope()
	scope.Where("id = ?", p.ID).First(&perf)
	ctx := context.Background()
	id := strconv.FormatInt(p.ID, 10)
	data := perf.SearchData()
	_, err = ES.Index().Index(PERFORM_INDEX).Type("perform").Id(id).BodyJson(data).Do(ctx)
	return
}

func (p *Perform) AfterSave() (err error) {
	return p.Reindex()
}

func (p *Perform) Url() string {
	if !p.HasAudio {
		panic("does not have audio")
	}
	return "http://classic-online.ru/a.php?file_id=" + p.ClassicOnlineId
}

func ListenedPerforms(cu *SiteUser) (ret []*Perform, err error) {
	ret = []*Perform{}
	performIDs := []int64{}
	if err = DB.Model(&Event{}).Where("site_user_id = ?", cu.ID).Select("DISTINCT(perform_id)").Limit(10).Pluck("perform_id", &performIDs).Error; err != nil {
		return
	}
	if len(performIDs) == 0 {
		return
	}
	err = PerformSearchScope().Where("id IN (?)", performIDs).Find(&ret).Error
	return
}

func FavoritePerformScope(cu *SiteUser) (scope *gorm.DB) {
	return PerformSearchScope().Where("id IN (SELECT perform_id from site_user_performs WHERE \"like\" = ? and site_user_id = ?)", true, cu.ID)
}

func AddedPerforms(cu *SiteUser) (ret []*Perform, err error) {
	ret = []*Perform{}
	err = FavoritePerformScope(cu).Order("date DESC").Limit(10).Find(&ret).Error
	return
}

func RecommendedPerforms(cu *SiteUser) (ret []*Perform, err error) {
	ret = []*Perform{}
	return
}

func (a *Perform) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Perform{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

package models

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Comment struct {
	ID              int64      `gorm:"primary_key" json:"id"`
	ClassicOnlineId string     `gorm:"unique_index" json:"classic_online_id"`
	Date            *time.Time `json:"date"`
	UserID          *int64     `gorm:"type:bigint REFERENCES users(id)" json:"user_id"`
	User            *User      `json:"user"`
	PerformID       *int64     `gorm:"type:bigint REFERENCES performs(id)" json:"perform_id"`
	Perform         *Perform   `json:"perform"`
	Likes           int64      `json:"likes"`
	Text            string     `json:"text"`
	Cite            string     `json:"cite"`
	CiteAuthor      string     `json:"cite_author"`
	Approval        *bool      `json:"approval"`
}

func (a *Comment) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Comment{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

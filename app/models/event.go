package models

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Event struct {
	ID         int64      `gorm:"primary_key" json:"id"`
	SiteUserID *int64     `gorm:"type:bigint REFERENCES site_users(id);index:idx_site_user_kind" json:"site_user_id"`
	PerformID  *int64     `gorm:"type:bigint REFERENCES performs(id);index" json:"perform_id"`
	SiteUser   *SiteUser  `json:"site_user"`
	Perform    *Perform   `json:"perform"`
	Kind       string     `json:"kind" gorm:"index:idx_site_user_kind"`
	Value      int64      `json:"value"`
	ClientTime *time.Time `json:"client_time"`
}

func (a *Event) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Event{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

func (a *Event) Authorize(scope *gorm.DB, cu *SiteUser) {
	if cu == nil {
		*scope = *scope.Where("1 = 0")
		return
	}
	*scope = *scope.Where("site_user_id = ?", cu.ID)
}

package models

type SearchSub struct {
	ID   int64 `gorm:"primary_key"`
	From string
	To   string
}

package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type User struct {
	ID              int64      `gorm:"primary_key" json:"id"`
	ClassicOnlineId string     `gorm:"unique_index" json:"classic_online_id"`
	Name            string     `json:"name"`
	Password        string     `json:"password"`
	Comments        []*Comment `json:"comments"`
}

func (a *User) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []User{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Piece struct {
	ID              int64      `gorm:"primary_key" json:"id"`
	ClassicOnlineId string     `gorm:"unique_index" json:"classic_online_id"`
	Name            string     `json:"name"`
	ComposerID      *int64     `gorm:"type:bigint REFERENCES composers(id)" json:"composer_id"`
	Composer        *Composer  `json:"composer"`
	Performs        []*Perform `json:"performs"`
}

func (a *Piece) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Piece{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

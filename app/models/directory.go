package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Directory struct {
	ID                 int64              `gorm:"primary_key" json:"id"`
	SiteUserID         *int64             `gorm:"type:bigint REFERENCES site_users(id)" json:"site_user_id"`
	SiteUser           *SiteUser          `json:"site_user"`
	Name               string             `json:"name"`
	PerformDirectories []PerformDirectory `json:"perform_directories"`
	Performs           []Perform          `gorm:"many2many:perform_directories;" json:"performs"`
}

type DirectoryApiData struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func (d *Directory) ApiData() DirectoryApiData {
	return DirectoryApiData{ID: d.ID, Name: d.Name}
}

func (a *Directory) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Directory{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

func (a *Directory) Authorize(scope *gorm.DB, cu *SiteUser) {
	if cu == nil {
		*scope = *scope.Where("1 = 0")
		return
	}
	*scope = *scope.Where("site_user_id = ?", cu.ID)
}

package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Text struct {
	ID      int64  `gorm:"primary_key" json:"id"`
	Key     string `json:"key"`
	Content string `json:"content"`
}

func (a *Text) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Text{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

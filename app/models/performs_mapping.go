package models

const PerformMapping = `
{
	"settings": {
		"analysis": {
			"filter": {
        "ru_stop": {
          "type": "stop",
          "stopwords": "_russian_"
        },
        "ru_stemmer": {
          "type": "stemmer",
          "language": "russian"
        }
      },
			"char_filter": {
				"yo_to_e": {
					"type": "mapping",
					"mappings": [
					"ё => е"
					]
				}
			},
      "analyzer": {
        "default": {
          "char_filter": [
            "html_strip",
						"yo_to_e"
          ],
          "tokenizer": "standard",
          "filter": [
            "lowercase",
            "ru_stop",
            "ru_stemmer"
          ]
        }
      }
		},
		"number_of_shards": 1,
		"number_of_replicas": 0
	},
	"mappings": {
		"properties": {
			"classic_online_id": {
				"type": "keyword"
			},
			"upload_date": {
				"type": "date"
			},
			"date.keyword": {
				"type": "date"
			},
			"user": {
				"type": "text"
			},
			"likes": {
				"type": "long"
			},
			"piece": {
				"type": "text"
			},
			"piece_id": {
				"type": "long"
			},
			"composer": {
				"type": "text"
			},
			"performers": {
				"type": "text"
			},
			"joined_performers": {
				"type": "text"
			},
			"has_audio": {
				"type": "boolean"
			},
			"has_audiok.keyword": {
				"type": "boolean"
			},
			"comments_count": {
				"type": "long"
			},
			"summary": {
				"type": "text"
			}
		}
	}
}
`

package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type SiteUser struct {
	ID       int64     `gorm:"primary_key" json:"id"`
	Email    *string   `gorm:"unique_index" json:"email"`
	Name     *string   `json:"name"`
	Login    *string   `gorm:"unique_index" json:"login"`
	Role     *string   `gorm:"role" json:"role"`
	Events   []Event   `json:"events" api:"-"`
	Performs []Perform `json:"-" api:"-"`
}

func (su *SiteUser) DisplayName() string {
	if su.Name == nil {
		return ""
	}
	return *su.Name
}

func (su *SiteUser) IsAdmin() bool {
	return su.Role != nil && *su.Role == "admin"
}

func (a *SiteUser) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []SiteUser{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

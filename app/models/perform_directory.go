package models

type PerformDirectory struct {
	ID          int64  `gorm: "primary_key"`
	PerformID   *int64 `gorm:"type:bigint REFERENCES performs(id)"`
	Perform     *Perform
	DirectoryID *int64 `gorm:"type:bigint REFERENCES directories(id)"`
	Directory   *Directory
}

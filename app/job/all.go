package job

import (
	"fmt"

	"gitlab.com/pipocavsobake/goclon/parser"
	"gitlab.com/pipocavsobake/goclon/progress"
)

func All(h progress.ProgressHandler) {
	cli := parser.NewClient()
	biggest_id, err := parser.BiggestID(cli)
	if err != nil {
		println(err.Error())
	}
	smallest_id := 0
	h.Init(biggest_id - smallest_id)
	for id := smallest_id + 1; id < biggest_id; id++ {
		uri := fmt.Sprintf("https://classic-online.ru/archive/?file_id=%d", id)
		parser.Page(cli, uri)
		h.Add(1)
	}
	h.Finish()
}

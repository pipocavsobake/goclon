package fragments

import (
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/progress"
)

func Load(root, ext string, h progress.ProgressHandler) {
	re := regexp.MustCompile(`000/([0-9]+(/[0-9]+)*)$`)
	ptrn := filepath.Join(root, "*", "*"+ext)
	println(ptrn)
	paths, err := filepath.Glob(ptrn)
	println(len(paths))
	if err != nil {
		h.Fail("glob for root " + root + " and ext " + ext + " " + err.Error())
		return
	}
	h.Init(len(paths))
	perform := models.Perform{}
	fragment := models.Fragment{}
	for _, path := range paths {
		idStr := strings.Replace(re.FindString(strings.TrimSuffix(path, ext)), "/", "", -1)
		id, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			h.Fail("parse id " + idStr + " " + err.Error())
			return
		}
		perform = models.Perform{}
		if err := models.DB.Where("id = ?", id).First(&perform).Error; err != nil {
			h.Fail("find perform " + strconv.FormatInt(id, 10) + " " + err.Error())
			return
		}
		if perform.ID != id {
			h.Fail("find perform ID != id " + strconv.FormatInt(perform.ID, 10) + " != " + strconv.FormatInt(id, 10))
			return
		}
		dat, err := ioutil.ReadFile(path)
		if err != nil {
			h.Fail("read file " + path + " " + err.Error())
			return
		}

		fragment = models.Fragment{}
		lines := strings.Split(string(dat), "\n")
		for _, line := range lines {
			duration, err := time.ParseDuration(line)
			if err != nil {
				h.Fail("parse duration " + line + " " + err.Error())
				return
			}
			end := duration.Nanoseconds() / 1000
			start := fragment.End
			fragment.ID = 0
			if err := models.DB.Where("perform_id = ? and end = ?", id, end).Find(&fragment).Error; err != nil {
				h.Fail("find fragment with perform_id " + strconv.FormatInt(id, 10) + " and end " + strconv.FormatInt(end, 10) + " " + err.Error())
				return
			}
			if fragment.ID != 0 {
				continue
			}
			fragment = models.Fragment{
				PerformID: id,
				Start:     start,
				End:       end,
			}
			if err := models.DB.Save(&fragment).Error; err != nil {
				h.Fail("save fragment with perform_id " + strconv.FormatInt(id, 10) + " and end " + strconv.FormatInt(end, 10) + " " + err.Error())
				return
			}
		}
		h.Add(1)
	}
}

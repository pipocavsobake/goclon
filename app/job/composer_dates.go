package job

import (
	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/parser"
	"gitlab.com/pipocavsobake/goclon/progress"
)

func ComposerDates(h progress.ProgressHandler) {
	scope := models.DB.Model(&models.Composer{})
	cli := parser.NewClient()
	models.EachComposer(scope, func(composer *models.Composer, i, total int) {
		if i == 0 {
			h.Init(total)
		} else {
			h.Set(i)
		}
		err := parser.ComposerPage(cli, composer)
		if err != nil {
			h.Fail(err.Error())
		}
	})
	h.Finish()
}

package job

import (
	"fmt"

	"gitlab.com/pipocavsobake/goclon/parser"
	"gitlab.com/pipocavsobake/goclon/progress"
)

func Range(start, end int) func(h progress.ProgressHandler) {
	return func(h progress.ProgressHandler) {
		if start >= end {
			h.Init(1)
			h.Fail("nothing to parse")
			return
		}
		cli := parser.NewClient()
		h.Init(end - start)
		for id := start; id < end; id++ {
			uri := fmt.Sprintf("https://classic-online.ru/archive/?file_id=%d", id)
			parser.Page(cli, uri)
			h.Add(1)
		}
		h.Finish()
	}
}

func RangeBack(start, end int) func(h progress.ProgressHandler) {
	return func(h progress.ProgressHandler) {
		if start >= end {
			h.Init(1)
			h.Fail("nothing to parse")
			return
		}
		cli := parser.NewClient()
		biggest_id, _ := parser.BiggestID(cli)
		smallest_id := biggest_id - end
		biggest_id -= start
		h.Init(biggest_id - smallest_id)
		for id := smallest_id + 1; id < biggest_id; id++ {
			uri := fmt.Sprintf("https://classic-online.ru/archive/?file_id=%d", id)
			parser.Page(cli, uri)
			h.Add(1)
		}
		h.Finish()
	}
}

package job

import (
	"gopkg.in/resty.v1"

	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/parser"
	"gitlab.com/pipocavsobake/goclon/progress"

	"strconv"
)

func Heads(h progress.ProgressHandler) {
	cli := parser.NewClient()
	cli.SetRedirectPolicy(resty.FlexibleRedirectPolicy(20))
	c := 0
	scope := models.DB.Model(&models.Perform{}).Where("has_audio = ? and (content_length is null or content_length = 0)", true).Order("id asc")
	scope.Count(&c)
	h.Init(c)
	fail := false
	models.EachPerform(scope, func(perform *models.Perform, i, total int) bool {
		if fail {
			return false
		}
		response, err := cli.R().Head(perform.Url())
		if err != nil {
			fail = true
			h.Fail(err.Error())
			return false
		}
		cl, _ := response.Header()["Content-Length"]
		if len(cl) > 0 { // TODO RETRY
			perform.ContentLength, _ = strconv.ParseInt(cl[0], 10, 64)
			models.DB.Save(perform)
		}
		h.Add(1)
		return true
	})
	h.Finish()
}

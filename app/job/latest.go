package job

import (
	"fmt"
	"strconv"

	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/parser"
	"gitlab.com/pipocavsobake/goclon/progress"
)

func Latest(h progress.ProgressHandler) {
	cli := parser.NewClient()
	biggest_id, _ := parser.BiggestID(cli)
	perf := models.Perform{}
	models.DB.Order("date desc").First(&perf)
	smallest_id, err := strconv.Atoi(perf.ClassicOnlineId)
	if err != nil {
		h.Init(1)
		h.Fail(err.Error())
		return
	}
	h.Init(biggest_id - smallest_id)
	for id := smallest_id + 1; id < biggest_id; id++ {
		uri := fmt.Sprintf("https://classic-online.ru/archive/?file_id=%d", id)
		parser.Page(cli, uri)
		h.Add(1)
	}
	h.Finish()
}

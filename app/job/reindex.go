package job

import (
	"context"
	"strconv"

	"github.com/olivere/elastic/v7"
	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/progress"
)

func pErr(err error) {
	if err != nil {
		panic(err)
	}
}

func dropIndex() {
	ctx := context.Background()
	exists, err := models.ES.IndexExists(models.PERFORM_INDEX).Do(ctx)
	pErr(err)
	if exists {
		_, err = models.ES.DeleteIndex(models.PERFORM_INDEX).Do(ctx)
		pErr(err)
	}
	createIndex, err := models.ES.CreateIndex(models.PERFORM_INDEX).BodyString(models.PerformMapping).Do(ctx)
	pErr(err)
	if !createIndex.Acknowledged {
		panic("not acknowledged")
	}
}

func updateES(performs []*models.Perform) {
	bulkRequest := models.ES.Bulk()
	for _, perform := range performs {
		id := strconv.FormatInt(perform.ID, 10)
		req := elastic.NewBulkIndexRequest().Index(models.PERFORM_INDEX).Type("perform").Id(id).Doc(perform.SearchData())
		bulkRequest = bulkRequest.Add(req)
	}
	_, err := bulkRequest.Do(context.Background())
	pErr(err)
}

func Reindex(h progress.ProgressHandler) {
	dropIndex()
	scope := models.PerformSearchScope()

	models.BulkPerform(scope, func(performs []*models.Perform, offset, total int) bool {
		if len(performs) == 0 {
			return false
		}
		if offset == 0 {
			h.Init(total)
		} else {
			h.Set(offset)
		}
		updateES(performs)
		return true
	})
	h.Finish()
}

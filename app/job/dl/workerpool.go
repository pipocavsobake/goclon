// Более сложный пример, с использованием пула обработчиков для типовых задач
package dl

import (
	"errors"
	"sync"
	"time"

	"gitlab.com/pipocavsobake/goclon/parser"
)

// Task - описание интрефейса работы
type Task struct {
	ID  int64
	Url string
}

// Pool - структура, нам потребуется Мутекс, для гарантий атомарности изменений самого объекта
// Канал входящих задач
// Канал отмены, для завершения работы
// WaitGroup для контроля завершнеия работ
type Pool struct {
	mu    sync.Mutex
	size  int
	tasks chan Task
	kill  chan struct{}
	ech   chan error
	sch   chan SavingItem
	wg    sync.WaitGroup
}

// Скроем внутреннее усройство за конструктором, пользователь может влиять только на размер пула
func NewPool(size int, sch chan SavingItem, ech chan error) *Pool {
	pool := &Pool{
		// Канал задач - буферизированный, чтобы основная программа не блокировалась при постановке задач
		tasks: make(chan Task, 128),
		// Канал kill для убийства "лишних воркеров"
		kill: make(chan struct{}),
		sch:  sch,
		ech:  ech,
	}
	// Вызовем метод resize, чтобы установить соответствующий размер пула
	pool.Resize(size)
	return pool
}

// Жизненный цикл воркера
func (p *Pool) worker() {
	loader := &StandardLoader{Client: parser.NewClient()}
	defer p.wg.Done()
	for {
		select {
		// Если есть задача, то ее нужно обработать
		case task, ok := <-p.tasks:
			if !ok {
				return
			}
			body, err := loader.Load(task.Url)
			if err != nil {
				p.ech <- errors.New("loaderError \"" + err.Error() + "\" for " + task.Url)
				continue
			}
			for len(p.sch) > 6 {
				time.Sleep(4 * time.Second)
			}
			p.sch <- SavingItem{ID: task.ID, FileName: FileName(task.ID, ".mp3"), Body: body, MsgForError: task.Url}
			// Если пришел сигнал умирать, выходим
		case <-p.kill:
			return
		}
	}
}

func (p *Pool) Resize(n int) {
	// Захватывам лок, чтобы избежать одновременного изменения состояния
	p.mu.Lock()
	defer p.mu.Unlock()
	for p.size < n {
		p.size++
		p.wg.Add(1)
		go p.worker()
	}
	for p.size > n {
		p.size--
		p.kill <- struct{}{}
	}
}

func (p *Pool) Close() {
	close(p.tasks)
}

func (p *Pool) Wait() {
	p.wg.Wait()
}

func (p *Pool) Exec(task Task) {
	p.tasks <- task
}

package dl

import (
	"gopkg.in/resty.v1"
)

// не thread-safe, можно использовать только в одной рутине
type DangerousLoader interface {
	Load(string) ([]byte, error)
}

type StandardLoader struct {
	Client *resty.Client
}

func (l *StandardLoader) Load(url string) ([]byte, error) {
	l.Client.SetRedirectPolicy(resty.FlexibleRedirectPolicy(20))
	response, err := l.Client.R().Get(url)
	if err != nil {
		return []byte{}, err
	}
	return response.Body(), err
}

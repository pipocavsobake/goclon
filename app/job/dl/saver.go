package dl

import (
	"errors"
	"io/ioutil"
	"os"
	pathlib "path"
)

type Saver interface {
	SetRoot(path string) error
	GetRoot() string
	Save(path string, body []byte) error
}

type StandardSaver struct {
	root string
}

func (s *StandardSaver) SetRoot(path string) error {
	stat, err := os.Stat(path)
	if err != nil {
		return err
	}
	if !stat.Mode().IsDir() {
		return errors.New(path + " is not a directory")
	}
	s.root = path
	return nil
}

func (s *StandardSaver) GetRoot() string {
	return s.root
}

func (s *StandardSaver) Save(path string, body []byte) error {
	fullpath := pathlib.Join(s.root, path)
	dir, _ := pathlib.Split(fullpath)
	if stat, err := os.Stat(dir); err != nil || !stat.Mode().IsDir() {
		err = os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			return err
		}

	}
	return ioutil.WriteFile(fullpath, body, 0644)
}

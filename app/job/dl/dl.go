package dl

import (
	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/progress"

	"sync"
)

func Dl(h progress.ProgressHandler) {
	c := 0
	err := models.DB.Exec("update performs set dl_status = null where dl_status = 'loading'").Error
	if err != nil {
		panic(err)
	}
	q := "has_audio = ? and (content_length is not null) and (dl_status is NULL)"
	scope := models.DB.Model(&models.Perform{}).Where(q, true).Order("likes desc")
	scope.Count(&c)
	h.Init(c)
	var wg sync.WaitGroup
	ch := make(chan SavingItem, 24)
	el := ListenError()
	ech := el.Chan()
	die := make(chan bool)
	NewSaveListener(&wg, ch, die, ech, h)
	pool := NewPool(12, ch, ech)
	batch_size := 100
	for {
		performs := []*models.Perform{}
		scope.Limit(batch_size).Find(&performs)
		if len(performs) == 0 {
			break
		}
		ids := make([]int64, len(performs))
		for i, perform := range performs {
			pool.Exec(Task{Url: perform.Url(), ID: perform.ID})
			ids[i] = perform.ID
		}
		err := models.DB.Exec("update performs set dl_status = 'loading' where id IN (?)", ids).Error
		if err != nil {
			ech <- err
		}
	}
	pool.Close()
	pool.Wait()
	die <- true
	wg.Wait()
	el.Kill()
	h.Finish()
}

package dl

import (
	"path"
	"strconv"
)

func FileName(id int64, ext string) string {
	numString := strconv.FormatInt(id, 10)
	for l := len(numString); l < 21; l++ {
		numString = "0" + numString
	}
	return path.Join(numString[:3], numString[3:6], numString[6:9], numString[9:12], numString[12:15], numString[15:18], numString[18:]) + ext
}

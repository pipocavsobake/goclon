package dl

import (
	"fmt"
	"sync"
)

type ErrorListener struct {
	ch  chan error
	kch chan struct{}
	wg  sync.WaitGroup
}

func ListenError() *ErrorListener {
	el := ErrorListener{}
	el.ch = make(chan error, 10)
	el.kch = make(chan struct{})
	go el.Run()
	return &el
}

func (el *ErrorListener) Run() {
	el.wg.Add(1)
	for {
		select {
		case err := <-el.ch:
			fmt.Println(err)
		case <-el.kch:
			el.wg.Done()
			return
		}
	}
}

func (el *ErrorListener) Kill() {
	el.kch <- struct{}{}
	el.wg.Wait()
}

func (el *ErrorListener) Chan() chan error {
	return el.ch
}

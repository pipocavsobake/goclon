package dl

import (
	"errors"
	"sync"

	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/progress"
)

type SavingItem struct {
	ID          int64
	Body        []byte
	FileName    string
	MsgForError string
}

type SaveListener struct {
	saver Saver
	ch    chan SavingItem
	die   chan bool
	wg    *sync.WaitGroup
	ech   chan error
	h     progress.ProgressHandler
}

func NewSaveListener(wg *sync.WaitGroup, ch chan SavingItem, die chan bool, ech chan error, h progress.ProgressHandler) *SaveListener {
	if wg == nil {
		panic("You must provide wait group")
	}
	ss := StandardSaver{}
	sl := SaveListener{wg: wg, ch: ch, die: die, saver: &ss, ech: ech, h: h}
	err := sl.saver.SetRoot("/data/c2")
	if err != nil {
		panic(err)
	}
	go sl.Run()
	return &sl
}

func (sl *SaveListener) Run() {
	sl.wg.Add(1)
	for {
		select {
		case item := <-sl.ch:
			if err := sl.saver.Save(item.FileName, item.Body); err != nil {
				sl.ech <- errors.New("\"" + err.Error() + "\" for " + item.MsgForError)
			}
			if err := models.DB.Exec("update performs set dl_status = 'done' where id = ?", item.ID).Error; err != nil {
				sl.ech <- errors.New("\"" + err.Error() + "\" for " + item.MsgForError)
			}
			sl.h.Add(1)
		case <-sl.die:
			sl.wg.Done()
			return
		}
	}
}

func (sl *SaveListener) Chan() chan SavingItem {
	return sl.ch
}

package workers

import (
	"github.com/qor/worker"
	"gitlab.com/pipocavsobake/goclon/config/admin"
)

var Worker *worker.Worker

func Init() {
	Worker = worker.New()
	InitParseWorker()
	admin.Admin.AddResource(Worker)
}

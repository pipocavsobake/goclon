package tests

import (
	"gitlab.com/pipocavsobake/goclon/app/models"
	"testing"
)

func TestCommentsCount(t *testing.T) {
	c := models.Comment{}
	models.DB.First(&c)
	p := c.Perform
	//models.DB.Where("id = ?", c.PerformID).First(&p)
	if p.CommentsCount() == 0 {
		t.Error("comments count is 0")
	}

}

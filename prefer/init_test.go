package prefer

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestSquareMatrix(t *testing.T) {
	m := NewSquareMinMatrix([]float64{0, 1, 3, 2, 0, 5, 2, 1, 0})
	spew.Dump(m)
	println(m.String())
}

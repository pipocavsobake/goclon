package prefer

import (
	"strconv"
	"strings"
)

type Scalar interface {
	Add(Scalar) Scalar
	Mul(Scalar) Scalar
	Zero() Scalar
	//Eye() Scalar
	//Opp() Scalar
	//Inv() Scalar
	String() string
}

type MinScalar float64

func (s MinScalar) Mul(b Scalar) Scalar {
	if s > b.(MinScalar) {
		return b
	}
	return s
}

func (s MinScalar) Add(b Scalar) Scalar {
	return s + b.(MinScalar)
}

func (s MinScalar) Zero() Scalar {
	return MinScalar(0)
}

func (s MinScalar) Eye() Scalar {
	return MinScalar(1)
}

func (s MinScalar) Opp() Scalar {
	return -s
}

func (s MinScalar) Inv() Scalar {
	return 1 / s
}

func (s MinScalar) String() string {
	return strconv.FormatFloat(float64(s), 'f', 2, 64)
}

type SquareMatrix []Scalar

func (m SquareMatrix) Rows() (i int) {
	for i = 0; i*i < len(m); i++ {
	}
	return
}

func (m SquareMatrix) Mul(b SquareMatrix) SquareMatrix {
	n := m.Rows()
	ret := make(SquareMatrix, n*n)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			ret[i*n+j] = m[0].Zero()
			for k := 0; k < n; k++ {
				ret[i*n+j] = ret[i*n+j].Add(m[i*n+k].Mul(b[k*n+j]))
			}
		}
	}
	return ret
}

func (m SquareMatrix) Add(b SquareMatrix) SquareMatrix {
	ret := make(SquareMatrix, len(m))
	for i := 0; i < len(m); i++ {
		ret[i] = m[i].Add(b[i])
	}
	return ret
}

func (m SquareMatrix) GeomSum(maxDeg int, transform func(SquareMatrix, int) SquareMatrix) SquareMatrix {
	ret := make(SquareMatrix, len(m))
	cur := make(SquareMatrix, len(m))
	for i := 0; i < len(cur); i++ {
		cur[i] = m[i]
		ret[i] = m[i]
	}
	for i := 1; i < maxDeg; i++ {
		cur = cur.Mul(m)
		ret = ret.Add(transform(cur, i))
	}
	return ret
}

func (m SquareMatrix) String() string {
	s := []string{}
	n := m.Rows()
	for i := 0; i < n; i++ {
		row := []string{}
		for j := 0; j < n; j++ {
			row = append(row, m[i*n+j].String())
		}
		s = append(s, strings.Join(row, ",\t"))
	}
	return strings.Join(s, "\n")
}

func NewSquareMinMatrix(data []float64) SquareMatrix {
	ret := SquareMatrix{}
	for _, datum := range data {
		var s Scalar = MinScalar(datum)
		ret = append(ret, s)
	}
	return ret
}

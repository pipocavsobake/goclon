package admin

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/qor/admin"
	"github.com/qor/qor"
	"github.com/qor/qor/resource"
	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/config/admin/actions"
	"gitlab.com/pipocavsobake/goclon/config/logger"
	auth "gitlab.com/pipocavsobake/goclon/config/siteauth"
)

var Admin *admin.Admin

type AdminAuth struct{}

func (AdminAuth) LoginURL(c *admin.Context) string {
	return "/auth/login"
}

func (AdminAuth) LogoutURL(c *admin.Context) string {
	return "/auth/logout"
}

func (AdminAuth) GetCurrentUser(c *admin.Context) qor.CurrentUser {
	currentUser := auth.Auth.GetCurrentUser(c.Request)
	if currentUser == nil || !currentUser.(*models.SiteUser).IsAdmin() {
		return nil
	}
	qorCurrentUser, ok := currentUser.(qor.CurrentUser)
	if !ok {
		logger.Logger.Printf("User %#v haven't implement qor.CurrentUser interface\n", currentUser)
	}
	return qorCurrentUser
}
func Init() {
	Admin = admin.New(&admin.AdminConfig{
		DB:   models.DB,
		Auth: &AdminAuth{},
	})
	Admin.SetSiteName("Classic-online Parser")

	Admin.AddResource(&models.User{})
	Admin.AddResource(&models.SiteUser{})
	Admin.AddResource(&models.Mark{})
	Admin.AddResource(&models.Comment{})
	Admin.AddResource(&models.SiteUserPerform{})
	article := Admin.AddResource(&models.Article{})
	article.Scope(&admin.Scope{
		Name:    "With SiteUser",
		Default: true,
		Handler: func(db *gorm.DB, context *qor.Context) *gorm.DB {
			return db.Preload("SiteUser")
		},
	})
	article.Meta(&admin.Meta{
		Name:      "Content",
		FieldName: "Content",
		Label:     "Content",
		Config:    &admin.RichEditorConfig{},
	})
	article.Meta(&admin.Meta{
		Name:      "Slug",
		FieldName: "Slug",
		Type:      "readonly",
	})
	article.Meta(&admin.Meta{
		Name:      "SiteUser",
		FieldName: "SiteUser",
		Setter: func(object interface{}, metaValue *resource.MetaValue, context *qor.Context) {
			object.(*models.Article).SiteUserId = &(context.CurrentUser).(*models.SiteUser).ID
		},
		FormattedValuer: func(record interface{}, context *qor.Context) (result interface{}) {
			//return record.(*models.SiteUser).ID
			//spew.Dump(record)
			a := record.(*models.Article)
			if a.SiteUser == nil {
				return fmt.Sprintf("%v", *a.SiteUserId)
			}
			return a.SiteUser.Email
		},
	})
	text := Admin.AddResource(&models.Text{})
	textMeta := text.GetMeta("Content")
	if textMeta == nil {
		panic("meta nil")
	}
	text.Meta(&admin.Meta{
		Name:      "Content",
		FieldName: "Content",
		Label:     "Content",
		Config:    &admin.RichEditorConfig{},
	})

	composer := Admin.AddResource(&models.Composer{})
	composer.Action(actions.ComposerDates)

	Admin.AddResource(&models.Group{})
	Admin.AddResource(&models.SearchSub{})

	perform := Admin.AddResource(&models.Perform{})
	perform.Action(actions.Reindex)
	perform.Action(actions.Latest)
	perform.Action(actions.All)
	actions.Range.Resource = Admin.AddResource(&actions.RangeArgument{})
	actions.RangeBack.Resource = actions.Range.Resource
	perform.Action(actions.Range)
	perform.Action(actions.RangeBack)

	Admin.AddResource(&models.Performer{})
	Admin.AddResource(&models.Piece{})
	Admin.AddResource(&models.Job{})
	Admin.AddResource(&models.Preference{})
}

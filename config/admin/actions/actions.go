package actions

import (
	"fmt"

	"github.com/qor/admin"
	"gitlab.com/pipocavsobake/goclon/app/job"
	"gitlab.com/pipocavsobake/goclon/app/models"
)

var ComposerDates *admin.Action
var Reindex *admin.Action
var Latest *admin.Action
var All *admin.Action
var Range *admin.Action
var RangeBack *admin.Action

type RangeArgument struct {
	Start, End int
}

func init() {
	ComposerDates = &admin.Action{
		Name: "dates",
		Handler: func(actionArgument *admin.ActionArgument) error {
			j := models.Job{Kind: "composer_dates"}
			go job.ComposerDates(&j)
			return nil
		},
		Modes: []string{"collection"},
	}

	Reindex = &admin.Action{
		Name: "reindex",
		Handler: func(actionArgument *admin.ActionArgument) error {
			j := models.Job{Kind: "perform_reindex"}
			go job.Reindex(&j)
			return nil
		},
		Modes: []string{"collection"},
	}

	Latest = &admin.Action{
		Name: "parse latest",
		Handler: func(actionArgument *admin.ActionArgument) error {
			j := models.Job{Kind: "parse_latest"}
			go job.Latest(&j)
			return nil
		},
		Modes: []string{"collection"},
	}

	All = &admin.Action{
		Name: "parse all",
		Handler: func(actionArgument *admin.ActionArgument) error {
			j := models.Job{Kind: "parse_all"}
			go job.All(&j)
			return nil
		},
		Modes: []string{"collection"},
	}

	Range = &admin.Action{
		Name: "parse range",
		Handler: func(actionArgument *admin.ActionArgument) error {
			arg := actionArgument.Argument.(*RangeArgument)
			j := models.Job{Kind: "parse_range", InputData: fmt.Sprintf("%d,%d", arg.Start, arg.End)}
			go job.Range(arg.Start, arg.End)(&j)
			return nil
		},
		Modes: []string{"collection"},
	}

	RangeBack = &admin.Action{
		Name: "parse range back",
		Handler: func(actionArgument *admin.ActionArgument) error {
			arg := actionArgument.Argument.(*RangeArgument)
			j := models.Job{Kind: "parse_range_back", InputData: fmt.Sprintf("%d,%d", arg.Start, arg.End)}
			go job.RangeBack(arg.Start, arg.End)(&j)
			return nil
		},
		Modes: []string{"collection"},
	}
}

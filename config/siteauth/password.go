package siteauth

import (
	"bytes"
	"io"
	"io/ioutil"
	"reflect"
	"strconv"
	"strings"

	"github.com/buger/jsonparser"
	"github.com/pkg/errors"
	"github.com/qor/auth"
	"github.com/qor/auth/claims"
	"github.com/qor/auth/providers/password"
	"github.com/qor/qor/utils"
	"github.com/qor/session"
	"gitlab.com/pipocavsobake/goclon/config/logger"
	"gitlab.com/pipocavsobake/goclon/config/siteauth/auth_identity"
)

var PasswordAuthorizeHandler = func(context *auth.Context) (*claims.Claims, error) {
	var (
		authInfo    auth_identity.Basic
		req         = context.Request
		tx          = context.Auth.GetDB(req)
		provider, _ = context.Provider.(*password.Provider)
	)

	req.ParseForm()

	var body []byte
	var err error
	body, req.Body, err = readBody(req.Body)
	if err != nil {
		return nil, errors.Wrap(err, "read body")
	}

	isJSON := strings.ToLower(req.Header.Get("Content-type")) == "application/json"

	authInfo.Provider = provider.GetName()
	if isJSON {
		authInfo.UID, err = jsonparser.GetString(body, "data", "login")
		if err != nil {
			return nil, errors.Wrap(err, "get data.login from json")
		}
	} else {
		authInfo.UID = strings.TrimSpace(req.Form.Get("login"))
	}

	if tx.Model(context.Auth.AuthIdentityModel).Where(authInfo).Scan(&authInfo).RecordNotFound() {
		return nil, errors.Wrap(auth.ErrInvalidAccount, "record not found")
	}

	if provider.Config.Confirmable && authInfo.ConfirmedAt == nil {
		currentUser, _ := context.Auth.UserStorer.Get(authInfo.ToClaims(), context)
		provider.Config.ConfirmMailer(authInfo.UID, context, authInfo.ToClaims(), currentUser)

		return nil, password.ErrUnconfirmed
	}
	var pw string
	if isJSON {
		pw, err = jsonparser.GetString(body, "data", "password")
		if err != nil {
			return nil, errors.Wrap(err, "get data.password from json")
		}
	} else {
		pw = strings.TrimSpace(req.Form.Get("password"))
	}

	if err := provider.Encryptor.Compare(authInfo.EncryptedPassword, pw); err == nil {
		return authInfo.ToClaims(), err
	} else {
		logger.Logger.Println("auth Encryptor error", err.Error())
	}

	return nil, auth.ErrInvalidPassword
}

func readBody(body io.ReadCloser) (parsed []byte, newBody io.ReadCloser, err error) {
	parsed, err = ioutil.ReadAll(body)
	newBody = ioutil.NopCloser(bytes.NewReader(parsed))
	return
}

func PasswordRegisterHandler(context *auth.Context) (*claims.Claims, error) {
	var (
		err         error
		currentUser interface{}
		schema      auth.Schema
		authInfo    auth_identity.Basic
		req         = context.Request
		tx          = context.Auth.GetDB(req)
		provider, _ = context.Provider.(*password.Provider)
	)

	req.ParseForm()

	var body []byte
	body, req.Body, err = readBody(req.Body)
	if err != nil {
		return nil, errors.Wrap(err, "read body")
	}

	isJSON := strings.ToLower(req.Header.Get("Content-type")) == "application/json"
	var login, pw string
	if isJSON {
		login, err = jsonparser.GetString(body, "data", "login")
		if err != nil {
			return nil, errors.Wrap(err, "get data.login from json")
		}
		pw, err = jsonparser.GetString(body, "data", "password")

		if err != nil {
			return nil, errors.Wrap(err, "get data.password from json")
		}

	} else {
		login = req.Form.Get("login")
		pw = req.Form.Get("password")
	}
	if login == "" {
		return nil, errors.Wrap(auth.ErrInvalidAccount, "login == \"\"")
	}

	if pw == "" {
		return nil, auth.ErrInvalidPassword
	}

	authInfo.Provider = provider.GetName()
	authInfo.UID = strings.TrimSpace(login)

	if !tx.Model(context.Auth.AuthIdentityModel).Where(authInfo).Scan(&authInfo).RecordNotFound() {
		return nil, errors.Wrap(auth.ErrInvalidAccount, "record not found")
	}

	if authInfo.EncryptedPassword, err = provider.Encryptor.Digest(strings.TrimSpace(pw)); err == nil {
		schema.Provider = authInfo.Provider
		schema.UID = authInfo.UID
		schema.Email = authInfo.UID
		schema.RawInfo = req

		var uid string
		currentUser, uid, err = context.Auth.UserStorer.Save(&schema, context)
		if err != nil {
			return nil, err
		}
		uidInt, err := strconv.ParseInt(uid, 10, 64)
		if err != nil {
			return nil, err
		}

		authInfo.UserID = &uidInt

		// create auth identity
		authIdentity := reflect.New(utils.ModelType(context.Auth.Config.AuthIdentityModel)).Interface()
		if err = tx.Where(authInfo).FirstOrCreate(authIdentity).Error; err == nil {
			if provider.Config.Confirmable {
				context.SessionStorer.Flash(context.Writer, req, session.Message{Message: password.ConfirmFlashMessage, Type: "success"})
				err = provider.Config.ConfirmMailer(schema.Email, context, authInfo.ToClaims(), currentUser)
			}

			return authInfo.ToClaims(), err
		}
	}

	return nil, err
}

package siteauth

import (
	"html/template"
	"io/ioutil"
	"net/http"
	"path/filepath"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/qor/auth"
	"github.com/qor/auth/providers/password"
	"github.com/qor/redirect_back"
	"github.com/qor/render"
	"github.com/qor/session/manager"
	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/assets"
	"gitlab.com/pipocavsobake/goclon/config/siteauth/auth_identity"
	yaml "gopkg.in/yaml.v2"
)

var Auth *auth.Auth

func Init() {
	authConfigPath := "./config/auth.yml"
	pwd, err := filepath.Abs("./")
	if filepath.Base(pwd) == "siteauth" {
		authConfigPath = "../auth.yml"
	}
	dat, err := ioutil.ReadFile(authConfigPath)
	if err != nil {
		panic(err)
	}
	var conf map[string]string
	err = yaml.Unmarshal([]byte(dat), &conf)
	if err != nil {
		panic(err)
	}
	ss, ok := conf["signed_string"]
	if !ok {
		panic("Need signed_string in config/auth.yml")
	}
	Auth = auth.New(&auth.Config{
		DB:        models.DB,
		UserModel: models.SiteUser{},

		LoginHandler:    LoginHandler,
		RegisterHandler: RegisterHandler,
		Render: render.New(&render.Config{
			AssetFileSystem: assets.AssetFS,
			FuncMapMaker: func(*render.Render, *http.Request, http.ResponseWriter) template.FuncMap {
				return template.FuncMap{
					"raw": func(str string) template.HTML { return template.HTML(str) },
					"adminText": func(name string) string {
						text := models.Text{}
						models.DB.Where("key = ?", name).First(&text)
						if text.ID == 0 {
							return "not Found"
						}
						return text.Content
					},
				}
			},
		}),

		SessionStorer: &auth.SessionStorer{
			SessionName:    "_rm_auth",
			SessionManager: manager.SessionManager,
			SigningMethod:  jwt.SigningMethodHS256,
			SignedString:   ss,
		},
		Redirector: auth.Redirector{redirect_back.New(&redirect_back.Config{
			SessionManager:  manager.SessionManager,
			IgnoredPrefixes: []string{"/auth"},
		})}})

	models.DB.AutoMigrate(&auth_identity.AuthIdentity{})

	Auth.RegisterProvider(password.New(&password.Config{
		AuthorizeHandler: PasswordAuthorizeHandler,
		RegisterHandler:  PasswordRegisterHandler,
	}))
}

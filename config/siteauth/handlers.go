package siteauth

import (
	"encoding/json"
	"html/template"
	"net/http"

	"gitlab.com/pipocavsobake/goclon/config/logger"

	"github.com/qor/auth"
	"github.com/qor/auth/claims"
	"github.com/qor/responder"
	"github.com/qor/session"
)

func respondAfterLogged(claims *claims.Claims, context *auth.Context) {
	// login user
	context.Auth.Login(context.Writer, context.Request, claims)

	responder.With("html", func() {
		// write cookie
		context.Auth.Redirector.Redirect(context.Writer, context.Request, "login")
	}).With([]string{"json"}, func() {
		storer := context.Config.SessionStorer.(*auth.SessionStorer)
		tokenString := storer.SessionManager.Get(context.Request, storer.SessionName)
		msg, e := json.Marshal(map[string]string{"token": tokenString})
		if e != nil {
			panic(e)
		}
		context.Writer.Write(msg)
	}).Respond(context.Request)
}

var LoginHandler = func(context *auth.Context, authorize func(*auth.Context) (*claims.Claims, error)) {
	var (
		req        = context.Request
		w          = context.Writer
		claim, err = authorize(context)
	)

	if err == nil && claim != nil {
		respondAfterLogged(claim, context)
		return
	}

	logger.Logger.Println("auth error", err)
	context.SessionStorer.Flash(w, req, session.Message{Message: template.HTML(err.Error()), Type: "error"})

	responder.With("html", func() {
		context.Auth.Config.Render.Execute("auth/login", context, req, w)
	}).With([]string{"json"}, func() {
		context.Writer.WriteHeader(http.StatusUnauthorized)
		msg, e := json.Marshal(map[string]string{"error": err.Error()})
		if e != nil {
			panic(e)
		}
		context.Writer.Write(msg)
	}).Respond(context.Request)
}

var RegisterHandler = func(context *auth.Context, register func(*auth.Context) (*claims.Claims, error)) {
	var (
		req         = context.Request
		w           = context.Writer
		claims, err = register(context)
	)

	if err == nil && claims != nil {
		respondAfterLogged(claims, context)
		return
	}

	context.SessionStorer.Flash(w, req, session.Message{Message: template.HTML(err.Error()), Type: "error"})

	// error handling
	responder.With("html", func() {
		context.Auth.Config.Render.Execute("auth/register", context, req, w)
	}).With([]string{"json"}, func() {
		context.Writer.WriteHeader(http.StatusUnprocessableEntity)
		msg, e := json.Marshal(map[string]string{"error": err.Error()})
		if e != nil {
			panic(e)
		}
		context.Writer.Write(msg)
	}).Respond(context.Request)
}

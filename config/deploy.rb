set :user, 'clonclient'
set :application, 'clonclient'
set :repo_url, 'git@gitlab.com:pipocavsobake/goclon.git'

set :branch, ENV['REVISION'] || 'master'

set :deploy_to, -> { "/home/#{fetch(:user)}/app" }

set :use_sudo, false
set :keep_releases, 10
set :linked_files, %w[config/database.yml config/headers.yml config/auth.yml]
set :linked_dirs, %w[tmp log]

namespace :go do
  desc 'TODO'
  task :local_build do
    puts 'local_build'
    `/usr/bin/zsh -c "bindatafs --exit-after-compile=false assets"`
    `/usr/bin/zsh -c "go run main.go compile-assets"`
    `/usr/bin/zsh -c "go build -tags 'bindatafs' -o tmp/clonclient"`
  end

  desc 'send locally built binary to server'
  task :upload do
    # https://stackoverflow.com/a/25558264/7419642
    on roles(:all) do
      upload! 'tmp/clonclient', "#{release_path}/clonclient"
    end
  end

  desc 'stop daemon'
  task :stop do
    on roles(:all) do
      execute "kill `cat #{release_path}/tmp/pid`"
    end
  end

  desc 'start daemon'
  task :start do
    on roles(:all) do
      execute "cd #{release_path} && ./clonclient serve --daemon"
      execute "cd #{release_path} && sleep 2  && chmod 777 tmp/sock"
    end
  end

  desc 'restart daemon'
  task :restart do
    on roles(:all) do
      invoke 'go:stop'
      invoke 'go:start'
    end
  end
end

before 'deploy:check:directories', 'go:local_build'
after 'git:create_release', 'go:upload'
after 'deploy:symlink:release', 'go:restart'

package database

import (
	"gitlab.com/pipocavsobake/goclon/config/logger"

	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	yaml "gopkg.in/yaml.v2"
)

// Database config
type Database struct {
	Adapter       *string
	Host          *string
	Encoding      *string
	Port          *int
	Pool          *int
	Username      *string
	Password      *string
	Db            *string `yaml:"database"`
	ElasticURL    *string `yaml:"elastic_url"`
	MappingPrefix *string `yaml:"mapping_prefix"`
}

// DB Gorm DB
var DB *gorm.DB

// Config database config data
var Config Database

// Configs possible DB configs
var Configs map[string]Database

func init() {
	var err error

	databaseConfigPath := "./config/database.yml"

	pwd, err := filepath.Abs("./")
	if filepath.Base(pwd) == "database" {
		databaseConfigPath = "../database.yml"
	} else if filepath.Base(pwd) == "tests" {
		databaseConfigPath = "../config/database.yml"
	} else if filepath.Base(pwd) == "parser" {
		databaseConfigPath = "../config/database.yml"
	} else if filepath.Base(pwd) == "ic" {
		databaseConfigPath = "../../config/database.yml"
	}

	dat, err := ioutil.ReadFile(databaseConfigPath)
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal([]byte(dat), &Configs)
	if err != nil {
		panic(err)
	}

	env := os.Getenv("RAILS_ENV")
	if env == "" {
		env = "development"
	}
	Config = Configs[env]

	if *Config.Adapter == "postgresql" {
		connstr := make([]string, 0)
		if Config.Host != nil {
			connstr = append(connstr, "host="+*Config.Host)
		}
		if Config.Port != nil {
			connstr = append(connstr, "port="+strconv.Itoa(*Config.Port))
		}
		if Config.Username != nil {
			connstr = append(connstr, "user="+*Config.Username)
		}
		if Config.Db != nil {
			connstr = append(connstr, "dbname="+*Config.Db)
		}
		if Config.Password != nil {
			connstr = append(connstr, "password="+*Config.Password)
		}
		connstr = append(connstr, "sslmode=disable")
		cs := strings.Join(connstr, " ")
		if logger.Logger != nil {
			logger.Logger.Printf("connecting to pg, conf: %v", cs)
		}
		DB, err = gorm.Open("postgres", cs)
		if err != nil {
			panic(err)
		}
	} else {
		panic("Unknown DB adapter: " + *Config.Adapter)
	}
	//log.Println("enable log mode")
	//DB.LogMode(true)
	DB.DB().SetMaxOpenConns(20)

	if Config.ElasticURL == nil {
		url := "http://localhost:9200"
		Config.ElasticURL = &url
	}
}

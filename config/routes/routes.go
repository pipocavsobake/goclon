package routes

import (
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/glebtv/gin-swagger"
	"github.com/glebtv/gin-swagger/swaggerFiles"

	"gitlab.com/pipocavsobake/goclon/app/controllers"
	apiController "gitlab.com/pipocavsobake/goclon/app/controllers/api"
	"gitlab.com/pipocavsobake/goclon/app/controllers/directories"
	"gitlab.com/pipocavsobake/goclon/assets"
	"gitlab.com/pipocavsobake/goclon/config/admin"
	auth "gitlab.com/pipocavsobake/goclon/config/siteauth"
	_ "gitlab.com/pipocavsobake/goclon/docs"
	"gitlab.com/pipocavsobake/goclon/logger"
)

var r *gin.Engine

func Router() *gin.Engine {
	if r != nil {
		return r
	}
	gin.DisableConsoleColor()
	f, _ := os.Create("log/gin.log")
	gin.DefaultWriter = io.MultiWriter(f)

	//config := cors.DefaultConfig()
	//config.AllowOrigins = []string{"http://localhost:3000", "https://clonclient.shirykalov.ru", "http://localhost:5292", "http://192.168.1.103:3000, https://stage1.shirykalov.ru"}
	//config.AllowCredentials = true
	//config.AllowMethods = []string{"GET", "PUT",}
	Auth := auth.Auth

	r = gin.Default()
	r.Use(logger.RequestLogger())
	//r.Use(cors.New(config))

	r.Any("/auth/*w", gin.WrapH(Auth.NewServeMux()))
	mux := http.NewServeMux()
	admin.Admin.MountTo("/admin", mux)
	r.Any("/admin/*w", gin.WrapH(mux))
	r.GET("/", controllers.HomeIndex)
	api := r.Group("/api")
	api.GET("/performs", controllers.PerformsIndex)             //+
	api.GET("/performs/favorite", controllers.PerformsFavorite) //+
	api.GET("/perform/:perform_id", controllers.PerformsShow)   //+
	api.PUT("/performs/:perform_id/:method", controllers.SiteUserPerformsUpdate)
	api.GET("/directories", controllers.DirectoriesIndex)                                         //+
	api.GET("/directories/:directory_id/performs", directories.MemberPerformsIndex)               //+
	api.POST("/directories", controllers.DirectoriesCreate)                                       //+
	api.DELETE("/directories/:id", controllers.DirectoriesDelete)                                 //+
	api.POST("/directory/:directory_id/performs/:perform_id", directories.MemberPerformsCreate)   //+
	api.DELETE("/directory/:directory_id/performs/:perform_id", directories.MemberPerformsDelete) //+
	api.POST("/events", controllers.CreateEvent)                                                  //+
	comments := api.Group("/comments")
	comments.PUT("", controllers.CommentsUpdate)
	comments.GET("", controllers.CommentsIndex)
	r.GET("/search", controllers.PerformsSearch)       //+
	api.GET("/search", controllers.PerformsApiSearch)  //+
	api.GET("/suggests", controllers.PerformsSuggests) //+

	fights := api.Group("/fights")
	fights.GET("", controllers.IndexFights)
	fights.POST("", controllers.CreateFight)
	fights.GET("/:id", controllers.ShowFight)
	fights.PUT("/:id", controllers.EditFight)

	api.GET("/panels", controllers.Panels)

	//apiV2 := api.Group("/v2")
	//apiV2Articles := apiV2.Group("/articles")
	//apiV2Articles.GET("", apiController.ArticlesIndex)

	//apiV2Composers := apiV2.Group("/composers")
	//apiV2Composers.GET("", apiController.ComposersIndex)

	//apiV2Pieces := apiV2.Group("/pieces")
	//apiV2Pieces.GET("", apiController.PiecesIndex)

	apiR := api.Group("/v2")
	for url, handler := range apiController.Index {
		apiR.GET(url, handler)
	}

	r.GET("/stylesheets/*w", gin.WrapH(assets.AssetFS.FileServer(http.Dir("public"))))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	for _, path := range []string{"system", "webpack", "images"} {
		r.Static("/"+path, "public/"+path)
	}

	r.NoRoute(controllers.HomeIndex)

	return r
}

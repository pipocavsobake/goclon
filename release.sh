rm -Rf ./assets && \
bindatafs --exit-after-compile=false assets && \
go run main.go compile-assets && \
go build -tags 'bindatafs' -o tmp/clonclient && \
#NODE_ENV=production yarn build && \
ssh_conn=clonclient@aesn.ru && \
current_path=/home/clonclient/app/current && \
rsync -avz --progress tmp/clonclient $ssh_conn:$current_path/clonclient && \
rsync -a --progress ./public/ $ssh_conn:$current_path/public/ && \
ssh $ssh_conn "systemctl --user restart clonclient" && \
rm public/webpack/*

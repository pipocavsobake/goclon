# Парсер комментариев с classic-online.ru
## Требования
postgresql, elasticaseach
## Usage
1. Скопируйте шаблоны файлов с настройками
```
cd config
cp database.yml.example database.yml
cp headers.yml.example headers.yml
cp auth.yml.example auth.yml
cd ..
```
Положите свои cookie в config/headers.yml, послед авторизации в браузере
2.Запуск проекта
```
go run main.go
```

## Build for deploy
```
bindatafs --exit-after-compile=false assets
go run main.go --compile
go build -tags 'bindatafs'
```

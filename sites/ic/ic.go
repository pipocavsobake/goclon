package ic

import (
	"errors"
	//"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.com/pipocavsobake/goclon/app/models"
	"gitlab.com/pipocavsobake/goclon/progress"

	"github.com/PuerkitoBio/goquery"
)

const host = "http://intoclassics.net"
const FirstPath = "/news/1-0-1"

var base *url.URL

func init() {
	var err error
	base, err = url.Parse(host)
	if err != nil {
		panic(err)
	}
}

func Resolve(pth string) (string, error) {
	uri, err := url.Parse(pth)
	if err != nil {
		return "", err
	}
	return base.ResolveReference(uri).String(), nil
}

func GetDoc(pth string) (*goquery.Document, error) {
	u, err := Resolve(pth)
	if err != nil {
		return nil, err
	}
	return goquery.NewDocument(u)
}

func GetBiggestLink(doc *goquery.Document) (link string, ok bool) {
	doc.Find("a.swchItem").Each(func(i int, s *goquery.Selection) {
		if i == 3 {
			link, ok = s.Attr("href")
		}
		if i == 5 {
			ok = false
		}
	})
	return
}

func PageLink(doc *goquery.Document) (link string, ok bool) {
	item := doc.Find("a.swchItem").Last()
	link, ok = item.Attr("href")
	if !strings.Contains(item.Text(), "»") {
		ok = false
	}
	return
}

func Ping() {
	doc, err := GetDoc("/news/1-0-1")
	if err != nil {
		panic(err)
	}
	href, ok := doc.Find("a.swchItem").Last().Attr("href")
	if ok {
		println(href)
	}
}

func pErr(err error) {
	if err != nil {
		panic(err)
	}
}

func UpdateUrlsFrom(pth string, onError func(error) (proceed bool)) (nextUrl string) {
	time.Sleep(2 * time.Second)
	doc, err := GetDoc(pth)
	if !onError(err) {
		return ""
	}
	var proceed = true
	doc.Find(".eTitle > a").Each(func(i int, s *goquery.Selection) {
		url, ok := s.Attr("href")
		if !ok {
			proceed = proceed && onError(errors.New("a without href"))
		}
		post := models.Post{}
		proceed = proceed && onError(models.DB.FirstOrCreate(&post, models.Post{Url: url}).Error)
	})
	if !proceed {
		return ""
	}
	url, ok := PageLink(doc)
	if !ok {
		onError(errors.New("ne ok on " + pth))
		return ""
	}
	return url
}

func UpdateUrls(h progress.ProgressHandler) {
	doc, err := GetDoc(FirstPath)
	if err != nil {
		panic(err)
	}
	biggest, ok := GetBiggestLink(doc)
	if ok != true {
		panic("ne ok")
	}
	total, err := strconv.Atoi(biggest[6 : len(biggest)-4])
	if err != nil {
		panic(err)
	}
	h.Init(total)
	for url := FirstPath; url != ""; url = UpdateUrlsFrom(url, func(err error) bool {
		if err != nil {
			panic(err)
			return false
		}
		return true
	}) {
		h.Add(1)
	}
	h.Finish()
}

func LoadContent(h progress.ProgressHandler) {
	//root := "tmp/content"
	scope := models.DB.Model(&models.Post{}).Where("content is null")
	total := 0
	if err := scope.Count(&total).Error; err != nil {
		panic(err)
	}
	for {
		posts := []*models.Post{}
		if err := scope.Limit(100).Find(&posts); err != nil {
			panic(err)
		}
		//for _, post := range posts {
		//url, err := Resolve(post.Url)
		//if err != nil {
		//panic(err)
		//}
		//resp, err := http.Get(url)
		//if err != nil {
		//panic(err)
		//}
		//}
	}
}

package ic

import (
	"strings"
	"testing"
)

func TestPing(t *testing.T) {
	Ping()
}

func tErr(t *testing.T, err error) {
	if err != nil {
		t.Error(err)
	}
}

func TestBiggestLink(t *testing.T) {
	doc, err := GetDoc(FirstPath)
	tErr(t, err)
	GetBiggestLink(doc)
}

func TestPageLink(t *testing.T) {
	doc, err := GetDoc(FirstPath)
	tErr(t, err)
	if url, ok := PageLink(doc); ok != true || !strings.Contains(url, "/news/2-0-1") {
		t.Error("invalid answer " + url)
	}
	url, ok := GetBiggestLink(doc)
	if ok != true {
		t.Error("no biggest link")
	}
	doc, err = GetDoc(url)
	tErr(t, err)
	if _, ok = PageLink(doc); ok == true {
		t.Error("The last page containes link to the next page!")
	}
}

func TestUpdateUrls(t *testing.T) {
	UpdateUrls()
}

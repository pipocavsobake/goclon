import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import HomeIcon from "@material-ui/icons/Home";
import HeartIcon from "@material-ui/icons/Home";
import ListIcon from "@material-ui/icons/List";
import SettingsIcon from "@material-ui/icons/Settings";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const styles = {
  list: {
    width: 250
  }
};

class SwipeableTemporaryDrawer extends React.Component {
  render() {
    const { classes } = this.props;
    const awesomeStyle = { fontSize: 24, transform: "translateX(3px)" };

    const sideList = (
      <div className={classes.list}>
        <List>
          <ListItem button onClick={() => this.props.select("/")}>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary={"Главная"} />
          </ListItem>
          <ListItem button onClick={() => this.props.select("/media")}>
            <ListItemIcon>
              <FontAwesomeIcon style={awesomeStyle} icon="award" />
            </ListItemIcon>
            <ListItemText primary={"Медиатека"} />
          </ListItem>
          <ListItem button onClick={() => this.props.select("/lists")}>
            <ListItemIcon>
              <ListIcon />
            </ListItemIcon>
            <ListItemText primary={"Списки"} />
          </ListItem>
          <ListItem button onClick={() => this.props.select("/settings")}>
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary={"Настройки"} />
          </ListItem>
        </List>
      </div>
    );

    return (
      <SwipeableDrawer
        open={this.props.open}
        onClose={this.props.onClose}
        onOpen={this.props.onOpen}
      >
        <div
          tabIndex={0}
          role="button"
          onClick={this.props.onClose}
          onKeyDown={this.props.onClose}
        >
          {sideList}
        </div>
      </SwipeableDrawer>
    );
  }
}

SwipeableTemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  onOpen: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  select: PropTypes.func.isRequired
};

export default withStyles(styles)(SwipeableTemporaryDrawer);

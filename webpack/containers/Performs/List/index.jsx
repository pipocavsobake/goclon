import React, { Component } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

class PerformsList extends Component {
  state = {
    openedMenu: null,
    anchorEl: null
  };
  toggleMenu(id, anchorEl) {
    if (this.state.openedMenu === id) {
      this.setState({ openedMenu: null, anchorEl: null });
    } else {
      this.setState({ openedMenu: id, anchorEl });
    }
  }
  selectSearch(kind, performer) {
    console.log("selected", kind, performer);
  }
  renderMenu(item) {
    return (
      <React.Fragment>
        <IconButton
          aria-label="More"
          aria-owns={open ? "long-menu" : undefined}
          aria-haspopup="true"
          onClick={event => this.toggleMenu(item.id, event.currentTarget)}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={this.state.anchorEl}
          open={this.state.openedMenu === item.id}
          onClose={() => this.toggleMenu(item.id)}
        >
          {item.performers.map(performer => (
            <MenuItem
              key={performer}
              selected={false}
              onClick={() => this.selectSearch("performer", performer)}
            >
              {`К исполнителю ${performer}`}
            </MenuItem>
          ))}
          <MenuItem
            selected={false}
            onClick={() => this.selectSearch("composer", item.composer)}
          >
            {`К композитору ${item.composer}`}
          </MenuItem>
          <MenuItem
            selected={false}
            onClick={() => this.selectSearch("piece_id", item.piece_id)}
          >
            {`К произведению ${item.piece}`}
          </MenuItem>
        </Menu>
      </React.Fragment>
    );
  }
  renderItem(item) {
    return (
      <ListItem button key={item.id}>
        <ListItemText
          primary={item.piece}
          secondary={
            <React.Fragment>
              <Typography component="span">{item.composer}</Typography>
              <Typography component="span">
                {item.performers.join(", ")}
              </Typography>
            </React.Fragment>
          }
        />
        <ListItemSecondaryAction>
          <Typography>15:34</Typography>
          {this.renderMenu(item)}
        </ListItemSecondaryAction>
      </ListItem>
    );
  }
  render() {
    return <List>{this.props.data.map(item => this.renderItem(item))}</List>;
  }
}

PerformsList.propTypes = {
  data: PropTypes.array.isRequired
};

export default PerformsList;

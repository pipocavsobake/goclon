import React, { Component } from "react";
import { Route, Switch, withRouter } from "react-router";
import Header from "~/containers/Header";
import HomeIndexPage from "~/pages/home/index";
import SettingsPage from "~/pages/settings";
import ArticlesIndexPage from "~/pages/articles/Index";

import { connect } from "react-redux";

class App extends Component {
  componentDidMount() {
    this.props.mount();
  }
  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <Switch>
            <Route exact path="/" component={HomeIndexPage} />
            <Route exact path="/settings" component={SettingsPage} />
            <Route exact path="/articles" component={ArticlesIndexPage} />
            <Route render={() => <div>Miss</div>} />
          </Switch>
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return { mount: () => dispatch({ type: "LOADED" }) };
}

function mapStateToProps(state) {
  return { switchKey: state.router.location.key };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import PropTypes from "prop-types";
import SideBar from "~/containers/Sidebar";

import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import { connect } from "react-redux";

import { push } from "connected-react-router";
import axios from "axios";

import Search from "~/containers/Search";

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1,
    cursor: "pointer"
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

class Header extends Component {
  state = {
    sidebar: false,
    pending: false,
    search: ""
  };
  logout() {
    if (this.state.pending) return;
    this.setState({ pending: true });
    axios
      .delete("/auth/logout")
      .then(resp => {
        this.props.logout();
        this.props.select("/users/sign_in");
      })
      .catch(err => {
        console.error(err);
        alert("Ты отсюда не уйдёшь");
      })
      .then(() => this.setState({ pending: false }));
  }
  toggleSideBar = target => {
    if (target === false || target === true) this.setState({ sidebar: target });
    else this.setState({ sidebar: !this.state.sidebar });
  };
  //suggest = value => {
  //return `/search?q=${value}`;
  //};
  //suggestions = resp => {
  //return resp.data.data.map(suggest => {
  //const summary = [
  //...suggest.performers,
  //suggest.composer,
  //suggest.piece
  //].join(" ");
  //return {
  //value: summary,
  //label: summary
  //};
  //});
  //};
  suggest = value => {
    return `/api/suggests?q=${value}`;
  };
  suggestions = resp => {
    return resp.data.map(s => ({ value: s, label: s }));
  };
  onSearch = value => {
    console.log("search", value);
  };
  render() {
    const { classes } = this.props;
    const logoutText = `Меня зовут ${this.props.currentUser &&
      this.props.currentUser.username} и я хочу выйти`;
    return (
      <header className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
            >
              <MenuIcon onClick={this.toggleSideBar} />
            </IconButton>
            <Typography
              variant="h6"
              color="inherit"
              className={classes.grow}
              onClick={() => this.props.select("/")}
            >
              Oh my music
            </Typography>
            <Search
              value={this.state.search}
              query={this.suggest}
              onChange={this.onSearch}
              options={this.suggestions}
            />
            {!this.props.currentUser ? (
              <Button
                onClick={() => this.props.select("/users/sign_in")}
                color="inherit"
              >
                Войти
              </Button>
            ) : (
              <Button onClick={() => this.logout()} color="inherit">
                {logoutText}
              </Button>
            )}
            <SideBar
              open={this.state.sidebar}
              select={this.props.select}
              onOpen={() => this.toggleSideBar(true)}
              onClose={() => this.toggleSideBar(false)}
            />
          </Toolbar>
        </AppBar>
      </header>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = ({ menu, currentUser }) => {
  return {
    menu,
    currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    select: path => dispatch(push(path)),
    logout: () => dispatch({ type: "LOGOUT" })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Header)
);

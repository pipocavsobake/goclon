import React from "react";
import { render } from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { Provider } from "react-redux";
import App from "./containers/App";
import reducer from "./reducer";
import { AppContainer } from "react-hot-loader";
import queryString from "query-string";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import { ConnectedRouter } from "connected-react-router";
import axiosMiddleware from "redux-axios-middleware";
import axios from "axios";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faAward } from "@fortawesome/free-solid-svg-icons";
library.add(faAward);

const client = axios.create({
  responseType: "json"
});

const init = () => {
  const history = createBrowserHistory();
  const store = createStore(
    reducer(history),
    composeWithDevTools(
      applyMiddleware(routerMiddleware(history)),
      applyMiddleware(axiosMiddleware(client))
    )
  );
  window.store = store;

  const r = () => {
    render(
      <AppContainer>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <App />
          </ConnectedRouter>
        </Provider>
      </AppContainer>,
      document.getElementById("root")
    );
  };

  r();

  if (module.hot) module.hot.accept("./containers/App", r);
};

document.addEventListener("DOMContentLoaded", init);

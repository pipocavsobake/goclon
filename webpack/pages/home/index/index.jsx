import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PerformsList from "~/containers/Performs/List";
import axios from "axios";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  }
});

class Index extends Component {
  state = {
    panels: [
      {
        title: "Прослушанное",
        data: []
      },
      {
        title: "Добавленное",
        data: []
      },
      {
        title: "Рекомендованное",
        data: []
      }
    ]
  };
  loadPerforms = () => {
    axios
      .get("/api/panels")
      .then(resp => {
        this.setState({ panels: resp.data });
      })
      .catch(err => {
        alert("Что-то пошло не так");
      });
  };

  componentDidMount() {
    this.loadPerforms();
  }

  renderPanel(panel) {
    return (
      <ExpansionPanel key={panel.title}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h5" component="h3">
            {panel.title}
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <PerformsList data={panel.data} />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Paper className={classes.root} elevation={1}>
          <Typography variant="h5" component="h3">
            Oh My Music
          </Typography>
          {this.state.panels.map(panel => this.renderPanel(panel))}
        </Paper>
      </div>
    );
  }
}

Index.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Index);

import React from "react";
import DataProvider from "~/containers/DataProvider";
import Table from "~/containers/Table";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import key from "weak-key";
const renderArticles = data => {
  return (
    <div>
      <Typography variant="h3" component="h3">
        Всего {data.total}
      </Typography>
      {data.data.map(article => (
        <Paper key={key(article)}>
          <Typography variant="h5" component="h3">
            {article.name}
          </Typography>
          <Typography
            component="p"
            dangerouslySetInnerHTML={{ __html: article.content }}
          />
        </Paper>
      ))}
    </div>
  );
};

const Articles = () => (
  <DataProvider
    endpoint="/api/v2/articles?includes=SiteUser"
    render={renderArticles}
  />
);

export default Articles;

package main

import (
	"fmt"
	"log"
	_ "net/http/pprof"

	//_ "github.com/motemen/go-loghttp/global"
	"github.com/gin-contrib/static"
	webpack "github.com/go-webpack/webpack"
	"gitlab.com/pipocavsobake/goclon/cli"
	"gitlab.com/pipocavsobake/goclon/config"
	"gitlab.com/pipocavsobake/goclon/config/logger"
	"gitlab.com/pipocavsobake/goclon/config/routes"
	"gitlab.com/pipocavsobake/goclon/docs"
)

// @title Clon
// @version 1.0
// @description API

// @BasePath /
// @host {{.Host}}
// swagger:meta

func main() {
	cli.Run()
	docs.SwaggerInfo.Host = cli.SWAG_HOST

	if cli.Serve {
		if cli.Daemon != nil {
			cntxt := cli.Daemon

			d, err := cntxt.Reborn()
			if err != nil {
				logger.Logger.Fatal("Unable to run: ", err)
			}
			if d != nil {
				return
			}
			defer cntxt.Release()

			logger.Logger.Print("- - - - - - - - - - - - - - -")
			logger.Logger.Print("daemon started")
		}
		cli.Prepare()
		webpack.DevHost = "localhost:7417"
		webpack.Plugin = "manifest"
		webpack.Verbose = false
		log.Println("webpack prod mode:", cli.WebpackProd)
		webpack.Init(!cli.WebpackProd)
		r := routes.Router()
		r.Use(
			static.Serve(
				"/webpack",
				static.LocalFile("/webpack", false),
			),
		)
		logger.Logger.Println(config.Config.Port)
		if cli.Daemon != nil {
			logger.Logger.Fatal(r.RunUnix(cli.Sock))
		} else {
			logger.Logger.Fatal(r.Run(fmt.Sprintf(":%v", config.Config.Port)))
		}
	}
	// // programatically set swagger info
	// docs.SwaggerInfo.Title = "Clon API"
	// docs.SwaggerInfo.Description = "API"
	// docs.SwaggerInfo.Version = "1.0"
	// docs.SwaggerInfo.Host = "localhost:5292"
	// docs.SwaggerInfo.BasePath = "/"
}

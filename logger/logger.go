package logger

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"

	"gitlab.com/pipocavsobake/goclon/config/logger"

	"github.com/gin-gonic/gin"
)

func RequestLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		buf, _ := ioutil.ReadAll(c.Request.Body)
		rdr1 := ioutil.NopCloser(bytes.NewBuffer(buf))
		rdr2 := ioutil.NopCloser(bytes.NewBuffer(buf)) //We have to create a new Buffer, because rdr1 will be read.
		s := readBody(rdr1)
		headers, err := json.MarshalIndent(c.Request.Header, "", "  ")
		if err != nil {
			logger.Logger.Println("marshalIndent Error", err.Error())
		} else {
		}
		logger.Logger.Println(string(headers))
		logger.Logger.Println("BODY:", s) // Print request body

		c.Request.Body = rdr2
		c.Next()
	}
}

func readBody(reader io.Reader) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(reader)

	s := buf.String()
	return s
}
